PUZ2
0 'IPUZ'
unknown1: 0, unknown2: 4, unknown3: 0
0000000100011111
I simply HATE droids! I need a DROID DETECTOR to make sure they don't come near me!
Thanks for the Droid Detector.
You look like someone who could use a DROID DETECTOR.

required: 0, placed: 434, character: 0 0000000000000000
1 'IPUZ'
unknown1: 0, unknown2: 4, unknown3: 0
0000000100011111
My hyperdrive needs a new ALLUVIAL DAMPER! Do you think you could find one for me?
My hyperdrive is as good as new!
In return, I can let you have this ALLUVIAL DAMPER... it's just the thing for your hyperdrive.

required: 0, placed: 435, character: 0 0000000000000000
2 'IPUZ'
unknown1: 0, unknown2: 4, unknown3: 0
0000000100011111
What I need is some TIBANNA GAS for some energy projects I'm working on.
May the Force be with you!
In return, I can let you have some rare TIBANNA GAS.

required: 0, placed: 436, character: 0 0000000000000000
3 'IPUZ'
unknown1: 0, unknown2: 4, unknown3: 0
0000000100011111
It can get pretty cold here! Have you got a spare THERMAL CAPE?
I feel warmer now.
In trade, I'll give you my THERMAL CAPE. It will keep you warm!

required: 0, placed: 437, character: 0 0000000000000000
4 'IPUZ'
unknown1: 0, unknown2: 2, unknown3: 0
0000000100011111
My droids are running amok! I need a RESTRAINING BOLT!
Thanks for the help!
In trade, I can give you this RESTRAINING BOLT. Useful for unruly droids!

required: 0, placed: 438, character: 0 0000000000000000
5 'IPUZ'
unknown1: 1, unknown2: 2, unknown3: 0
0000000100011111


I've got a RESTRAINING BOLT that will stop any droid. It's yours if you help me.

required: 0, placed: 438, character: 0 0000000000000000
6 'IPUZ'
unknown1: 0, unknown2: 2, unknown3: 0
0000000100011111
My old POWER TERMINAL has overloaded! Could you find me a new one?
Now I'm back in business, thanks to you!
I can let you have a POWER TERMINAL if you'll help me. They don't make them like this anymore.

required: 0, placed: 439, character: 0 0000000000000000
7 'IPUZ'
unknown1: 1, unknown2: 2, unknown3: 0
0000000100011111


Help me, and I'll hand over this POWER TERMINAL. You can use it to run just about anything.

required: 0, placed: 439, character: 0 0000000000000000
8 'IPUZ'
unknown1: 0, unknown2: 1, unknown3: 0
0000000100011111
My landspeeder went into a ditch! What I need is a POWER PRYBAR!
Thanks for your help!
If it's leverage you want, it's a POWER PRYBAR you need!

required: 0, placed: 440, character: 0 0000000000000000
9 'IPUZ'
unknown1: 1, unknown2: 1, unknown3: 0
0000000100011111


Help me out and I'll give you a POWER PRYBAR. As tools go, it's the best.

required: 0, placed: 440, character: 0 0000000000000000
10 'IPUZ'
unknown1: 0, unknown2: 4, unknown3: 0
0000000100011111
I'm never going to get off this planet without a NAVICOMPUTER! Can you help me?
Thank you! I thought I was stuck here!
Get me what I need, and I've got a NAVICOMPUTER for you. Don't jump to hyperspace without one!

required: 0, placed: 441, character: 0 0000000000000000
11 'IPUZ'
unknown1: 0, unknown2: 4, unknown3: 0
0000000100011111
I'm looking for some DURASTEEL-- all I need is a small BAR.
This is just what I needed.
All I can offer in return is a BAR of DURASTEEL, but who knows? Maybe you need some...

required: 0, placed: 443, character: 0 0000000000000000
12 'IPUZ'
unknown1: 0, unknown2: 4, unknown3: 0
0000000100011111
Any idea where I could get a SHIELD GENERATOR? My old one fried its capacitor.
Thanks for your help!
Help me and I'll help you... with a SHIELD GENERATOR, say?

required: 0, placed: 444, character: 0 0000000000000000
13 'IPUZ'
unknown1: 0, unknown2: 4, unknown3: 0
0000000100011111
My ship needs an overhaul. Without a DRIVE COMPENSATOR I'll be stalled for weeks!
You've done me a great favor, young Jedi.
You'll never get off this planet without a working DRIVE COMPENSATOR. Mine's yours if you help me...

required: 0, placed: 445, character: 0 0000000000000000
14 'IPUZ'
unknown1: 0, unknown2: 4, unknown3: 0
0000000100011111
My ship's COMM UNIT burned out. I need a new one.
Thanks for lending a hand.
How about a trade for my old COMM UNIT?

required: 0, placed: 446, character: 0 0000000000000000
15 'IPUZ'
unknown1: 0, unknown2: 2, unknown3: 0
0000000100011111
I've got a new lift engine, but the ANTI-GRAV GENERATOR is the wrong size!
Thanks for the Generator, sir!
I've got an extra ANTI-GRAV GENERATOR I'm offering for your help.

required: 0, placed: 447, character: 0 0000000000000000
16 'IPUZ'
unknown1: 0, unknown2: 4, unknown3: 0
0000000100011111
Information is my business-- I'm looking for DATA!
Thanks for the Data Card, young Skywalker.
In return you can have this DATA CARD. I'm not sure what's on it.

required: 0, placed: 449, character: 0 0000000000000000
17 'IPUZ'
unknown1: 0, unknown2: 4, unknown3: 0
0000000100011111
Hello! I'm trying to cast some transparisteel, and I don't have any LOMMITE.
Thanks for the Lommite; now I can get back to work.
Help me out, and I'll give you some LOMMITE CRYSTALS... all the way from Elom.

required: 0, placed: 450, character: 0 0000000000000000
18 'IPUZ'
unknown1: 0, unknown2: 4, unknown3: 0
0000000100011111
It's vital to my work that I get a sample of RYLL.
Now I can continue my experiment.
Solve my problem, and this sample of RYLL is yours... it's worth a bundle on the invisible market!

required: 0, placed: 452, character: 0 0000000000000000
19 'IPUZ'
unknown1: 0, unknown2: 4, unknown3: 0
0000000100011111
I need a SENSOR ARRAY!
Thanks for the Sensor Array.
If you come up with one, I'll give you a SENSOR ARRAY.

required: 0, placed: 453, character: 0 0000000000000000
20 'IPUZ'
unknown1: 0, unknown2: 2, unknown3: 0
0000000100011111
I need a small REPULSOR engine, and quick!
You've helped me immeasurably, young man.
How about a REPULSOR for your help? I've got a good one...

required: 0, placed: 454, character: 0 0000000000000000
21 'IPUZ'
unknown1: 1, unknown2: 2, unknown3: 0
0000000100011111


Find what I need and I'd be willing to part with my REPULSOR-- it's genuine Corellian.

required: 0, placed: 454, character: 0 0000000000000000
22 'IPUZ'
unknown1: 0, unknown2: 4, unknown3: 0
0000000100011111
Ever try to find your way through HYPERSPACE without a COMPASS? Well, you can't, and I need one!
Thank you! This will speed my journey!
Do what you can, and this HYPERSPACE COMPASS is all yours!

required: 0, placed: 455, character: 0 0000000000000000
23 'IPUZ'
unknown1: 0, unknown2: 1, unknown3: 0
0000000100011111
I'm in the market for a FUSION WELDER-- do you know where I could pick one up?
This Fusion Welder's just what I needed. Thanks!
Maybe you could use a FUSION WELDER. How about a trade?

required: 0, placed: 456, character: 0 0000000000000000
24 'IPUZ'
unknown1: 1, unknown2: 1, unknown3: 0
0000000100011111


Bring me what I need, and my FUSION WELDER is yours. I don't need it anymore.

required: 0, placed: 456, character: 0 0000000000000000
25 'IPUZ'
unknown1: 0, unknown2: 1, unknown3: 0
0000000100001111
I'm out here mining for phobium, and I need a BEAMDRILL.
Thanks for the Beamdrill.
If you can help me, I'll gladly hand over my BEAMDRILL.

required: 0, placed: 457, character: 0 0000000000000000
26 'IPUZ'
unknown1: 1, unknown2: 1, unknown3: 0
0000000100011111


When you want to disintegrate something, a BEAMDRILL is your best friend. You can take mine if you help me...

required: 0, placed: 457, character: 0 0000000000000000
27 'IPUZ'
unknown1: 0, unknown2: 2, unknown3: 0
0000000100011111
My droid can't talk! Have you got a spare VOCABULATOR?
Thanks! Now I can fix my droid.
For droids with speech problems, I've got just the VOCABULATOR you need. Worth a trade?

required: 0, placed: 458, character: 0 0000000000000000
28 'IPUZ'
unknown1: 1, unknown2: 2, unknown3: 0
0000000100011111


I've got a spare VOCABULATOR that I don't need. Maybe we can make a trade.

required: 0, placed: 458, character: 0 0000000000000000
29 'IPUZ'
unknown1: 0, unknown2: 4, unknown3: 0
0000000100011111
They're after me! I need a SENSOR PACK so I'll know when they're close.
No one's going to sneak up on me now!
And I'll bet you could use a SENSOR PACK. It's yours if you bring me what I need...

required: 0, placed: 460, character: 0 0000000000000000
30 'IPUZ'
unknown1: 0, unknown2: 1, unknown3: 0
0000000100011111
Hey, listen-- I need a DECODER. Strictly for legal use, mind you.
I don't let it get around, but I'm secretly a Rebel too. Travel safely!
You know, a good DECODER comes in handy. I've got one I can spare in return for some help.

required: 0, placed: 462, character: 0 0000000000000000
31 'IPUZ'
unknown1: 1, unknown2: 1, unknown3: 0
0000000100011111


You wouldn't be in the market for a DECODER, by any chance? Help me out, and it's yours.

required: 0, placed: 462, character: 0 0000000000000000
32 'IPUZ'
unknown1: 0, unknown2: 4, unknown3: 0
0000000100011111
A HOLOCUBE-- that's what I'm looking for!
Thanks for the Holocube!
All I can offer in return is a HOLOCUBE. I don't even know if it's recordable.

required: 0, placed: 463, character: 0 0000000000000000
33 'IPUZ'
unknown1: 0, unknown2: 4, unknown3: 0
0000000100011111
I need a hyperspace TRANSPONDER! My communication systems are useless without it!
Thanks for the equipment.
Help me and I'll help you... I've got a ship's hyperspace TRANSPONDER I'd be willing to part with.

required: 0, placed: 464, character: 0 0000000000000000
34 'IPUZ'
unknown1: 0, unknown2: 2, unknown3: 0
0000000100011111
My R5 unit has picked up a slight flutter, and I need a DROID PART.
Thanks for the thingamabob.
I don't have a lot to offer, except for a spare DROID PART I've got lying around. What do you say?

required: 0, placed: 465, character: 0 0000000000000000
35 'IPUZ'
unknown1: 1, unknown2: 2, unknown3: 0
0000000100011111


A spare DROID PART is all I can offer in return. Can you use it?

required: 0, placed: 465, character: 0 0000000000000000
36 'IPUZ'
unknown1: 0, unknown2: 4, unknown3: 0
0000000100011111
I need a POWER CONVERTER.
Thanks for the gizmo!
Find what I need, and you can have my POWER CONVERTER.

required: 0, placed: 466, character: 0 0000000000000000
37 'IPUZ'
unknown1: 0, unknown2: 2, unknown3: 0
0000000100011111
My R2 unit's got a bad motivator! Any idea where I could find a new one?
Thanks! My R2 unit will roll again!
In trade, I offer you a MOTIVATOR-- it's designed for an R2 unit, but it should work in almost anything.

required: 0, placed: 467, character: 0 0000000000000000
38 'IPUZ'
unknown1: 1, unknown2: 2, unknown3: 0
0000000100011111


Tell you what-- I've got an extra MOTIVATOR you can have if you help me. Pep up any droid!

required: 0, placed: 467, character: 0 0000000000000000
39 'IPUZ'
unknown1: 0, unknown2: 2, unknown3: 0
0000000100011111
My droid needs a new COMPUTER PROBE.
May the Force be with you.
If you can help me, I'll give you a networkable COMPUTER PROBE!

required: 0, placed: 469, character: 0 0000000000000000
40 'IPUZ'
unknown1: 1, unknown2: 2, unknown3: 0
0000000100011111


I have a COMPUTER PROBE that's network-ready! It's yours if you help me.

required: 0, placed: 469, character: 0 0000000000000000
41 'IPUZ'
unknown1: 0, unknown2: 1, unknown3: 0
0000000100011111
I need a DROID CALLER to summon my droids! Can you find one for me?
Those droids better watch themselves now!
Maybe we can make a deal... I've got a DROID CALLER that controls uppity droids. Want to trade?

required: 0, placed: 470, character: 0 0000000000000000
42 'IPUZ'
unknown1: 1, unknown2: 1, unknown3: 0
0000000100011111


In return, let me give you this DROID CALLER-- it will keep your droids in line...

required: 0, placed: 470, character: 0 0000000000000000
43 'IPUZ'
unknown1: 0, unknown2: 4, unknown3: 0
0000000100011111
There's a SPICE ring operating here, and I'm trying to break it up! I need a sample to identify the lab... really!
You fool, I'll make a fortune on the invisible market!
For your trouble, I can pass along a CRATE that came my way.  Who knows what's in it?

required: 0, placed: 471, character: 0 0000000000000000
44 'IPUZ'
unknown1: 0, unknown2: 0, unknown3: 0
0000000100011110
I'm locked out of my own house! You haven't seen a BLUE KEY CARD anywhere around here, have you?
Oh thank you, young Jedi-- I'll try not to lose it again!
You help me, I'll help you! A certain BLUE KEY CARD fell into my hands... maybe you could use it.

required: 0, placed: 472, character: 0 0000000000000000
45 'IPUZ'
unknown1: 1, unknown2: 0, unknown3: 0
0000000100011110


Please help me! You can have my BLUE KEY CARD if you do!

required: 0, placed: 472, character: 0 0000000000000000
46 'IPUZ'
unknown1: 0, unknown2: 0, unknown3: 0
0000000100011110
I need a RED KEY CARD. Strictly legal usage, I assure you!
Thanks for the key card. I can use it!
I'm sure you can help me. In return, I'll turn over this RED KEY CARD I found... deal?

required: 0, placed: 473, character: 0 0000000000000000
47 'IPUZ'
unknown1: 1, unknown2: 0, unknown3: 0
0000000100011110


If you don't help me, I don't know what I'll do! Look, you can have my RED KEY CARD, what about that?

required: 0, placed: 473, character: 0 0000000000000000
48 'IPUZ'
unknown1: 0, unknown2: 0, unknown3: 0
0000000100011110
I've got to get my hands on a GREEN KEY CARD! Never mind why...
Now I've got what I need. You're on your own.
I've got a GREEN KEY CARD-- who knows what it opens, but it might come in handy... trade?

required: 0, placed: 474, character: 0 0000000000000000
49 'IPUZ'
unknown1: 1, unknown2: 0, unknown3: 0
0000000100011110


There's got to be something I can give you in return... how about this GREEN KEY CARD?

required: 0, placed: 474, character: 0 0000000000000000
50 'IPUZ'
unknown1: 0, unknown2: 4, unknown3: 0
0000000100011111
I'm studying to be a Jedi, but I don't have a TRAINING REMOTE! Have you got one?
Thanks for the remote! Look out galaxy, here I come!
I've got a TRAINING REMOTE that could sharpen your skills, young Jedi. Help me out and it's yours.

required: 0, placed: 475, character: 0 0000000000000000
51 'IPUZ'
unknown1: 0, unknown2: 2, unknown3: 0
0000000100011111
I've got to find a LOCOMOTOR for my protocol droid; he just stands there!
My droid can walk again, thanks to you!
In exchange, I'll hand over a LOCOMOTOR that will make your droids hop!

required: 0, placed: 476, character: 0 0000000000000000
52 'IPUZ'
unknown1: 1, unknown2: 2, unknown3: 0
0000000100011111


If you're talking servos, I've got a LOCOMOTOR that will move any droid on the market. Bring me what I need and it's yours.

required: 0, placed: 476, character: 0 0000000000000000
53 'IPUZ'
unknown1: 0, unknown2: 1, unknown3: 0
0000000100011110
Blast! I locked my keys in my speeder and I need an ELECTRO- LOCKPICK.
So long, stranger, I've got work to do.
You need an ELECTRO- LOCKPICK, am I right? Solve my problem, and I'll hand it over.

required: 0, placed: 477, character: 0 0000000000000000
54 'IPUZ'
unknown1: 1, unknown2: 1, unknown3: 0
0000000100011110


Find what I need, and you can have my ELECTRO LOCKPICK...

required: 0, placed: 477, character: 0 0000000000000000
55 'IPUZ'
unknown1: 0, unknown2: 4, unknown3: 0
0000000100011111
Hello, stranger. I'm in need of a GLOW ROD.
Thanks. No more dark shadows!
All I can offer in trade is this GLOW ROD, but you never know when you might find yourself in the dark...

required: 0, placed: 478, character: 0 0000000000000000
56 'IPUZ'
unknown1: 0, unknown2: 4, unknown3: 0
0000000100011111
I'm marooned on this forsaken planet, unless I find a POWER COUPLING to repair my ship.
May the Force be with you!
I've got a POWER COUPLING for you... if you help me!

required: 0, placed: 479, character: 0 0000000000000000
57 'IPUZ'
unknown1: 0, unknown2: 4, unknown3: 0
0000000100011111
I'm working on a very important experiment, and I need a sample of CARBONITE. Get me some!
Now my experiment can continue. Thanks for the carbonite!
In return for your help, I've got a sample of CARBONITE for you. It's the real thing!

required: 0, placed: 481, character: 0 0000000000000000
58 'IPUZ'
unknown1: 0, unknown2: 4, unknown3: 0
0000000100011111
Maybe you can help me... I need a NAV CARD showing this sector.
Thanks for the Nav Card.
I'll trade you a NAV CARD for some help. It shows this whole sector.

required: 0, placed: 484, character: 0 0000000000000000
59 'IPUZ'
unknown1: 0, unknown2: 4, unknown3: 0
0000000100011111
I need a HOMING BEACON to signal my ship! My crew doesn't even know I'm here!
You've done me a great favor-- Thank you!
Do what you can for me, and there's a HOMING BEACON in it for you...

required: 0, placed: 485, character: 0 0000000000000000
60 'IPUZ'
unknown1: 0, unknown2: 2, unknown3: 0
0000000100011111
I deal in used ship parts... and right now, I'm in the market for a DRIVE GUIDE. Let me know if you spot one!
A pleasure doing business with you!
In return, if you need a DRIVE GUIDE, you can have mine.

required: 0, placed: 486, character: 0 0000000000000000
61 'IPUZ'
unknown1: 1, unknown2: 2, unknown3: 0
0000000100011111


Now, if you're looking for a DRIVE GUIDE, get me what I need, and I've got one for you..

required: 0, placed: 486, character: 0 0000000000000000
62 'IPUZ'
unknown1: 0, unknown2: 4, unknown3: 0
0000000100011111
Greetings! Do you know where I might find an ELECTROSCOPE?
Thanks for the 'scope!
Help me out, and I'll give you my father's old ELECTROSCOPE.

required: 0, placed: 487, character: 0 0000000000000000
63 'IPUZ'
unknown1: 0, unknown2: 4, unknown3: 0
0000000100011111
I do some hunting, but I can't hit a thing. I need a RANGEFINDER to improve my accuracy.
Hey, thanks for the Rangefinder!
In exchange, you can have the RANGEFINDER off my old Bryar pistol.

required: 0, placed: 488, character: 0 0000000000000000
64 'IPUZ'
unknown1: 0, unknown2: 4, unknown3: 0
0000000100011111
I need a CONDENSER UNIT. Can you find one for me?
Good luck on your mission.
In return, I can give you a pretty nice CONDENSER UNIT.

required: 0, placed: 489, character: 0 0000000000000000
65 'IPUZ'
unknown1: 0, unknown2: 1, unknown3: 0
0000000100011111
A MACROFUSER sure would come in handy... have you got one?
That's what I need... a Macrofuser!
I have a MACROFUSER that could come in handy... it's yours if you help me.

required: 0, placed: 491, character: 0 0000000000000000
66 'IPUZ'
unknown1: 1, unknown2: 1, unknown3: 0
0000000100011111


And for you I've got a MACROFUSER... for all your welding needs!

required: 0, placed: 491, character: 0 0000000000000000
67 'IPUZ'
unknown1: 0, unknown2: 1, unknown3: 0
0000000100010111
I need a HYDROSPANNER for some repairs I'm doing.
Thanks for the tool!
Meanwhile, a good HYDROSPANNER is mighty handy. Mine's yours if you help me.

required: 0, placed: 492, character: 0 0000000000000000
68 'IPUZ'
unknown1: 1, unknown2: 1, unknown3: 0
0000000100011111


If you're looking for a  HYDROSPANNER, I'll trade you.

required: 0, placed: 492, character: 0 0000000000000000
69 'IPUZ'
unknown1: 0, unknown2: 1, unknown3: 0
0000000100011111
You know those special BELTS Imperial stormtroopers wear? I want one!
Watch out for stormtroopers, kid.
Listen, I found an IMPERIAL BELT. I'd rather not keep it, but I can't just give it away...

required: 0, placed: 493, character: 0 0000000000000000
70 'IPUZ'
unknown1: 0, unknown2: 2, unknown3: 0
0000000100011111
If I had a small, portable GENERATOR, all my problems would be solved!
Thanks! Now I've got power on demand!
For you, I've got a portable GENERATOR. It's small, but powerful.

required: 0, placed: 494, character: 0 0000000000000000
71 'IPUZ'
unknown1: 1, unknown2: 2, unknown3: 0
0000000100011111


If you make it worth my while, I've got a portable GENERATOR for you.

required: 0, placed: 494, character: 0 0000000000000000
72 'IPUZ'
unknown1: 0, unknown2: 4, unknown3: 0
0000000100011111
I'm studying the nocturnal habits of mynoks... but I need a MACROSCOPE. They ate my first one!
Now I can continue my research.
Help me out and you can have my MACROSCOPE.

required: 0, placed: 495, character: 0 0000000000000000
73 'IPUZ'
unknown1: 0, unknown2: 4, unknown3: 0
0000000100011111
I've got a lot of tools, but nothing to store them in! I need a UTILITY BELT.
Thanks for the belt!
I'll trade for my UTILITY BELT; it's well-equipped!

required: 0, placed: 496, character: 0 0000000000000000
74 'IPUZ'
unknown1: 0, unknown2: 4, unknown3: 0
0000000100011111
I'm looking for a portable FUSION FURNACE, so I can recharge my droids.
Be careful. This is a dangerous place!
In exchange for your help, you can have my  FUSION FURNACE... it's a portable recharging station.

required: 0, placed: 497, character: 0 0000000000000000
75 'IPUZ'
unknown1: 1, unknown2: 2, unknown3: 0
0000000100011111


Let's make a deal-- find me what I need, and I'll give you my FUSION FURNACE. It's a portable recharging station.

required: 0, placed: 497, character: 0 0000000000000000
76 'IPUZ'
unknown1: 0, unknown2: 4, unknown3: 0
0000000100011111
You know the thing that broadcasts your ship ID? A TELESPONDER? I need one of those...
Thanks for the gadget.
Do me this favor, and I've got a brand new TELESPONDER for you.

required: 0, placed: 498, character: 0 0000000000000000
77 'IPUZ'
unknown1: 0, unknown2: 1, unknown3: 0
0000000100011111
I happen to be in need of a BREATH MASK... don't ask why.
Thanks. I'll breathe easier now!
And you look like someone who might want a BREATH MASK... and why not? They can sure come in handy!

required: 0, placed: 499, character: 0 0000000000000000
78 'IPUZ'
unknown1: 1, unknown2: 1, unknown3: 0
0000000100011111


Sometimes a BREATH MASK can come in handy, know what I mean? So let's trade!

required: 0, placed: 499, character: 0 0000000000000000
79 'IPUZ'
unknown1: 0, unknown2: 4, unknown3: 0
0000000100011111
Rumor has the Emperor shutting down the Holonet, but I'm going to fight back, and I need HOLOCOMM to do it.
Long live the Rebellion!
You may know a use for this HOLOCOMM. Give me what I want, and you can have it.

required: 0, placed: 500, character: 0 0000000000000000
80 'IPUZ'
unknown1: 0, unknown2: 4, unknown3: 0
0000000100011111
You look like a man who knows his way around. Can you find me a blank TRANSFER REGISTER?
Now leave me alone.
In return, I'll give you a brand new, blank TRANSFER REGISTER. A smuggler might find it valuable...

required: 0, placed: 501, character: 0 0000000000000000
81 'IPUZ'
unknown1: 0, unknown2: 1, unknown3: 0
0000000100011111
I need a COMLINK; can you help me?
Thanks for the Comlink.
I've got a COMLINK that's tapped into the Imperial frequencies... Interested?

required: 0, placed: 503, character: 0 0000000000000000
82 'IPUZ'
unknown1: 1, unknown2: 1, unknown3: 0
0000000100011111


I'm sure you can help me. If you do, I'll give you a COMLINK that can tap into Imperial frequencies!

required: 0, placed: 503, character: 0 0000000000000000
83 'IPUZ'
unknown1: 2, unknown2: 4, unknown3: 0
0000000010000000
Bring me Han Solo!
You've got your pal. Now be on your way!
You'd better bring me what I want, if you hope to see your friend HAN SOLO again...

required: 0, placed: 809, character: 0 0000000000000000
84 'IPUZ'
unknown1: 2, unknown2: 2, unknown3: 0
0000000010000000
This thing won't work without a DRIVE GUIDE!
May the Force be with you!
Solve my problem, and I'll help to wrap up this little adventure of yours...

required: 0, placed: 486, character: 0 0000000000000000
85 'IPUZ'
unknown1: 3, unknown2: 4, unknown3: 4
0000000000000000

Well done...
Free the FALCON, Luke!
Hello, Luke!

Troubled I am. Interrupt your training we must!

Your mercenary friend, HAN SOLO, knows little of the Force. Yet vital to the Rebellion have he and his ship become. Why this is so is beyond a Jedi Master's understanding.

No matter! Jabba's agents have impounded the MILLENNIUM FALCON on Tattooine... again!

Rescue him you must, and free the Falcon!

Here's something that will help you complete the task...
required: 0, placed: 810, character: 816 0000001100110000
86 'IPUZ'
unknown1: 1, unknown2: 1, unknown3: 4294967295
0000000100011111


If you lend a hand, there's an IM MINE in it for you... it can really blow things up!

required: 0, placed: 515, character: 0 0000000000000000
87 'IPUZ'
unknown1: 1, unknown2: 1, unknown3: 4294967295
0000000100011111


One good turn deserves another, as they say... so I'll give you a Jeron FUSION CUTTER for your trouble.

required: 0, placed: 518, character: 0 0000000000000000
88 'IPUZ'
unknown1: 1, unknown2: 1, unknown3: 4294967295
0000000100011111


If you can help me out, a GAS GRENADE will be your reward...

required: 0, placed: 430, character: 0 0000000000000000
89 'IPUZ'
unknown1: 0, unknown2: 1, unknown3: 4294967295
0000000100011111
I'm in desperate need of a GAS GRENADE... don't ask me why!
Thank you, stranger!
For your trouble, a GAS GRENADE will be yours.

required: 0, placed: 430, character: 25956 0110010101100100
90 'IPUZ'
unknown1: 1, unknown2: 1, unknown3: 4294967295
0000000100011111


Could you use a SMOKE GRENADE? Well I've got one with your name on it, if you help me...

required: 0, placed: 431, character: 0 0000000000000000
91 'IPUZ'
unknown1: 0, unknown2: 1, unknown3: 4294967295
0000000100011111
Look kid, find me a SMOKE GRENADE, you hear?
Good work, kid!
There's a SMOKE GRENADE in it for you, if you help me out.

required: 0, placed: 431, character: 0 0000000000000000
92 'IPUZ'
unknown1: 1, unknown2: 1, unknown3: 4294967295
0000000100011111


I've got an old tool called a SONIC CLEAVER that you might find useful. How about a trade?

required: 0, placed: 432, character: 0 0000000000000000
93 'IPUZ'
unknown1: 0, unknown2: 1, unknown3: 4294967295
0000000100011111
I'm in the market for a tool that uses sound to cut-- it's called a SONIC CLEAVER.
Thanks for the Cleaver!
I'll bet you could find a use for this SONIC CLEAVER I've got here... Trade?

required: 0, placed: 432, character: 0 0000000000000000
94 'IPUZ'
unknown1: 1, unknown2: 1, unknown3: 4294967295
0000000100011111


And I've got something useful... an ICE DRILL. Let's make a deal, huh?

required: 0, placed: 433, character: 0 0000000000000000
95 'IPUZ'
unknown1: 0, unknown2: 1, unknown3: 4294967295
0000000100011111
My snow-speeder got buried in an avalanche, and I need an ICE DRILL to chip it out!
May the Force be with you!
All I can offer in trade is a rusty old ICE DRILL... what do you say?

required: 0, placed: 433, character: 0 0000000000000000
96 'IPUZ'
unknown1: 1, unknown2: 1, unknown3: 4294967295
0000000100011111


And if you can help me out, you'll be the new owner of a genuine STASIS MINE. Do what you like with it!

required: 0, placed: 523, character: 29806 0111010001101110
97 'IPUZ'
unknown1: 2, unknown2: 4, unknown3: 4294967295
0000000010000000
I want a Jawa.

For your trouble, here's my little friend, Umlat. He will help you complete your mission.

required: 0, placed: 1350, character: 0 0000000000000000
98 'IPUZ'
unknown1: 2, unknown2: 2, unknown3: 4294967295
0000000010000000
I want an ENERGY RELAY.

Help me out, and I'll give you an ENERGY RELAY!

required: 0, placed: 1351, character: 26144 0110011000100000
99 'IPUZ'
unknown1: 2, unknown2: 2, unknown3: 4294967295
0000000010000000
I'm trying to patch up a protocol droid, and I need a DROID BODY.

If you succeed, I've got an old DROID BODY you can have. I bought it from some Jawa. Pretty good scrap!

required: 0, placed: 1357, character: 0 0000000000000000
100 'IPUZ'
unknown1: 2, unknown2: 0, unknown3: 4294967295
0000000010000000

Long live the Rebellion!
Tell you what-- find what I need and I'll give you a certain KEY CARD that should come in very handy...

required: 0, placed: 473, character: 0 0000000000000000
101 'IPUZ'
unknown1: 2, unknown2: 4, unknown3: 4294967295
0000000010000000

You have what you came for! Go... and may the Force be with you.
We can do each other a favor... get me what I need, and I'll let you take this CRATE OF SPICE off my hands-- it makes me nervous just having it around!

required: 0, placed: 471, character: 0 0000000000000000
102 'IPUZ'
unknown1: 1, unknown2: 4, unknown3: 4294967295
0000000100011111


Help me... 10,000 CREDITS will be your reward!

required: 0, placed: 429, character: 147 0000000010010011
103 'IPUZ'
unknown1: 3, unknown2: 4, unknown3: 4
0000000000000000

Well done, Luke! The Rebel Alliance is still alive!
Rescue GENERAL MARUTZ, Luke!
There you are, Luke! Heard my call you did, yes!

GENERAL MARUTZ, a leader of the Rebellion, has been kidnapped by stormtroopers. Danger there is, mmm?

Fly you must to the frigid snow planet Etorasp, and RESCUE the General... before Vader tortures the Rebel battle plans out of him!

Here! Take this... help you it will!
required: 0, placed: 1378, character: 1379 0000010101100011
104 'IPUZ'
unknown1: 2, unknown2: 0, unknown3: 4294967295
0000000010000000

Good luck with your mission!
Help me, and I'll give you a GREEN KEY CARD... who knows what it will open!

required: 0, placed: 474, character: 0 0000000000000000
105 'IPUZ'
unknown1: 2, unknown2: 1, unknown3: 4294967295
0000000010000000

Good luck!
In return for your help, you can have this ELECTRO LOCKPICK... It can open almost anything!

required: 0, placed: 477, character: 27680 0110110000100000
106 'IPUZ'
unknown1: 2, unknown2: 4, unknown3: 4294967295
0000000010000000

You're almost to the end of your quest, young Jedi!
In return for your help, I'll lend you my THERMAL CAPE. I'm warm enough, and it might just solve your problems...

required: 0, placed: 437, character: 0 0000000000000000
107 'IPUZ'
unknown1: 2, unknown2: 1, unknown3: 4294967295
0000000010000000

You've got what you need to solve your problem now!
I'm sure you can be of help in this matter. If so, I'm willing to trade you an ICE DRILL. What do you say?

required: 0, placed: 433, character: 0 0000000000000000
108 'IPUZ'
unknown1: 3, unknown2: 4, unknown3: 4
0000000000000000

Placebo text.
Destroy the HIDDEN FACTORY, Luke!
Luke! Time it is for your training to advance a step...

The Empire has set up a HIDDEN FACTORY in the snowy wastes of planet Neshtab, where they are building stormtrooper droids. These robots could tip the balance against the Rebel Alliance.

Find this factory you must! Destroy it you must!

Here! Help you finish what you start, this will...
required: 0, placed: 1419, character: 1420 0000010110001100
109 'IPUZ'
unknown1: 2, unknown2: 4, unknown3: 4294967295
0000000010000000

Go, young man, and may the Force be with you!
Prove to me you're loyal to the Rebellion by getting me what I need, and I'll hand over the means to finish your quest.

required: 0, placed: 429, character: 0 0000000000000000
110 'IPUZ'
unknown1: 2, unknown2: 4, unknown3: 4294967295
0000000010000000

Be brave! I feel you are almost at the end of your journey...
I have an extremely rare BLUMFRUIT to offer for your help. It's value goes beyond mere nutrition!

required: 0, placed: 483, character: 0 0000000000000000
111 'IPUZ'
unknown1: 2, unknown2: 1, unknown3: 4294967295
0000000010000000

I thank you! On with your travels!
If you can help me, I can help you! I have a GRAPPLING HOOK that may come in very handy...

required: 0, placed: 1292, character: 0 0000000000000000
112 'IPUZ'
unknown1: 0, unknown2: 1, unknown3: 4294967295
0000000100111111
Hey there, kid! You don't know where I could find a GRAPPLING HOOK, do you?
Thanks, kid.
Help me out here, and I'll trade you a GRAPPLING HOOK for your trouble. Deal?

required: 0, placed: 1292, character: 27491 0110101101100011
113 'IPUZ'
unknown1: 1, unknown2: 1, unknown3: 4294967295
0000000100011111

Thank you.
And for you there's a GRAPPLING HOOK. Help me... I suspect you're going to need it!

required: 0, placed: 1292, character: 202 0000000011001010
114 'IPUZ'
unknown1: 2, unknown2: 4, unknown3: 4294967295
0000000010000000

You have what you need now... onward!
I'm in desperate need of help! All I can offer in return is this SENSOR PACK, but I'm sure you will find a use for it!

required: 0, placed: 460, character: 0 0000000000000000
115 'IPUZ'
unknown1: 3, unknown2: 4, unknown3: 4
0000000000000000

This space for rent.
Rescue HAN, Luke!
Mmm! Opportunity do I sense for a young Jedi-in- training...

Your friend Han, even more impetuous than yourself, has been frozen in carbonite, as he probably deserves.

But now Jabba the Hutt has foolishly relaxed his vigilance. A chance to free Solo there is, yes!

Fly to Tattooine you must! Take advantage of the moment, yes!

Mmm, nearly forgot! Take this! Help you it will...
required: 0, placed: 1596, character: 1597 0000011000111101
116 'IPUZ'
unknown1: 2, unknown2: 4, unknown3: 4294967295
0000000010000000

Now you're ready to take on the Empire!
How about a trade? I just picked up this POWER CONVERTER from Tacchi Station.

required: 0, placed: 466, character: 28001 0110110101100001
117 'IPUZ'
unknown1: 2, unknown2: 4, unknown3: 4294967295
0000000010000000

Good luck with that Holocron.
I'm sure you can help me... and vice versa. I've got a rare item that's too hot for me to handle... and I'm ready to trade!

required: 0, placed: 461, character: 0 0000000000000000
118 'IPUZ'
unknown1: 2, unknown2: 0, unknown3: 4294967295
0000000010000000

Put that key to good use!
Do me this favor, and I'll turn over a KEY CARD that fell into my hands... I'm sure you need it!

required: 0, placed: 1243, character: 204 0000000011001100
119 'IPUZ'
unknown1: 0, unknown2: 0, unknown3: 4294967295
0000000100110111
I need a PURPLE KEY CARD! It's the highest priority!
Thanks for the Key!
If you find what I need, I'll be willing to hand over the PURPLE KEY CARD I found...

required: 0, placed: 508, character: 29806 0111010001101110
120 'IPUZ'
unknown1: 2, unknown2: 1, unknown3: 4294967295
0000000010000000

Good luck with your mission!
You know, I've got an old LADDER. Maybe you could use it... Help me, and it's yours!

required: 0, placed: 1246, character: 0 0000000000000000
121 'IPUZ'
unknown1: 2, unknown2: 4, unknown3: 4294967295
0000000010000000

I wish you luck!
Help me, and I'll give you a HOLOCUBE I've been saving.

required: 0, placed: 463, character: 0 0000000000000000
122 'IPUZ'
unknown1: 2, unknown2: 2, unknown3: 4294967295
0000000010000000

Good luck with that droid!
You know, I've got the HEAD of a useless old protocol droid. It's yours if you can help!

required: 0, placed: 502, character: 0 0000000000000000
123 'IPUZ'
unknown1: 2, unknown2: 0, unknown3: 4294967295
0000000010000000

Find a use for that key!
You look like a resourceful lad, so I'm sure you can help me. I'm offering an interesting KEY CARD in exchange.

required: 0, placed: 1244, character: 0 0000000000000000
124 'IPUZ'
unknown1: 2, unknown2: 4, unknown3: 4294967295
0000000010000000

I wish you well on your quest!
If you help me, I can give you a POWER COUPLING that will help wrap up your quest!

required: 0, placed: 479, character: 0 0000000000000000
125 'IPUZ'
unknown1: 2, unknown2: 1, unknown3: 4294967295
0000000010000000

I know you'll find a use for that hydrospanner!
All I can offer in return is an old HYDRO- SPANNER, but I'll bet it's just what you need!

required: 0, placed: 492, character: 0 0000000000000000
126 'IPUZ'
unknown1: 2, unknown2: 1, unknown3: 4294967295
0000000010000000

Careful with that mine!
Look, I happen to know you're searching for an IM MINE, but until you help me, you aren't going to get one!

required: 0, placed: 515, character: 0 0000000000000000
127 'IPUZ'
unknown1: 2, unknown2: 2, unknown3: 4294967295
0000000010000000

Handle that charge carefully!
You know, I've got a SEQUENCER CHARGE, and just having it around makes me nervous... let's make a trade!

required: 0, placed: 526, character: 0 0000000000000000
128 'IPUZ'
unknown1: 2, unknown2: 2, unknown3: 4294967295
0000000010000000

Be wary! There are Imperial spies everywhere...
I have something vital to your mission, but you've got to help me first! Trust me!

required: 0, placed: 527, character: 0 0000000000000000
129 'IPUZ'
unknown1: 2, unknown2: 1, unknown3: 4294967295
0000000010000000

May the fusion be with you... or whatever you Jedi always say!
I've got some old tools lying around I'd like to trade... and you need a FUSION WELDER, right?

required: 0, placed: 456, character: 0 0000000000000000
130 'IPUZ'
unknown1: 2, unknown2: 0, unknown3: 4294967295
0000000010000000

You've almost completed your task! Good luck!
I have a REBEL ID CARD lying around here somewhere... I'm sure to find it by the time you fulfill my request!

required: 0, placed: 1245, character: 0 0000000000000000
131 'IPUZ'
unknown1: 3, unknown2: 4, unknown3: 4
0000000000000000

This text is not here...
FIND LEIA you must... you are her only hope!
Luke!

A great disturbance in the Force there is... Princess Leia is MISSING! Her ship has been shot down on the forest planet Nibiru. Only you can save her!

This you will need...
required: 0, placed: 1617, character: 1618 0000011001010010
132 'IPUZ'
unknown1: 3, unknown2: 4, unknown3: 4
0000000000000000

Look elsewhere for this text.
Take the IMPERIAL BATTLE CODE from Ensign Waldron, Luke!
Luke! Again the dark side threatens...

At the Imperial Base in the sands of planet Argavat toils a brave officer who is secretly a Rebel spy. Ensign Waldron is his name. Stolen the IMPERIAL BATTLE CODE has he!

Fly to this desert world and relieve him of his burden you must... before Vader finds him!

Take this! Without it you cannot succeed...
required: 0, placed: 1788, character: 1789 0000011011111101
133 'IPUZ'
unknown1: 3, unknown2: 4, unknown3: 4
0000000000000000

do not read this
Destroy the RELAY STATION in the Tarsa System, Luke!
Another ripple in the Force, Luke...

Tireless are the minions of the Emperor, mmm? On the forest world of Tarsa, a RELAY STATION have they erected. The Rebel cause cannot prevail while it remains in place. Find it you must and destroy it!

This will help you...
required: 0, placed: 1828, character: 1829 0000011100100101
134 'IPUZ'
unknown1: 3, unknown2: 4, unknown3: 4
0000000000000000

Moon base... right!
Destroy the IMPERIAL BATTLE STATION in the Varn system, Luke!
Ahh, there you are, Luke! The dark side never rests...

Although the Death Star is no more, already the Empire has built another tool of terror and destruction on the airless moon of Varn. To destroy this new BATTLE STATION is your task...

Take this, it will help you...
required: 0, placed: 1650, character: 1651 0000011001110011
135 'IPUZ'
unknown1: 3, unknown2: 4, unknown3: 4
0000000000000000

After Solved Text:
Find the hidden base and WARN THE REBELS, Luke!
Luke! Great danger there is!

The Imperial Fleet has learned of the HIDDEN REBEL BASE on icy planet Thaldo. Attack is their plan! You must WARN the Rebels, Luke!

Only this can I give you...
required: 0, placed: 1652, character: 1653 0000011001110101
136 'IPUZ'
unknown1: 1, unknown2: 0, unknown3: 4294967295
0000000100110111

Life is better now!
You've got to help me! You're my only hope! I'll give you a KEY CARD if you succeed!

required: 0, placed: 508, character: 2979 0000101110100011
137 'IPUZ'
unknown1: 1, unknown2: 0, unknown3: 4294967295
0000000100110111

Thanks for the Key Card.
Maybe we could make some kind of trade... I have a YELLOW KEY CARD you might want...

required: 0, placed: 509, character: 0 0000000000000000
138 'IPUZ'
unknown1: 0, unknown2: 0, unknown3: 4294967295
0000000100110111
I seem to have lost my YELLOW KEY CARD. If you come across it, I'd sure like to have it back.
Thanks for being honest!
You can take my YELLOW KEY CARD in exchange!

required: 0, placed: 509, character: 40960 1010000000000000
139 'IPUZ'
unknown1: 1, unknown2: 4, unknown3: 4294967295
0000000100110111


You look like someone who could use a DROID DETECTOR, am I right? Let's make a trade!

required: 0, placed: 434, character: 8306 0010000001110010
140 'IPUZ'
unknown1: 1, unknown2: 4, unknown3: 4294967295
0000000100110111


In return, I can let you have this ALLUVIAL DAMPER... it's just the thing for a hyperdrive with too much ion flux.

required: 0, placed: 435, character: 29545 0111001101101001
141 'IPUZ'
unknown1: 1, unknown2: 4, unknown3: 4294967295
0000000100110111


I have some rare TIBANNA GAS... maybe you'd like to trade?

required: 0, placed: 436, character: 8306 0010000001110010
142 'IPUZ'
unknown1: 1, unknown2: 4, unknown3: 4294967295
0000000100110111


Meanwhile, are you warm enough? I've got a THERMAL CAPE I'd be willing to trade...

required: 0, placed: 437, character: 30496 0111011100100000
143 'IPUZ'
unknown1: 1, unknown2: 4, unknown3: 4294967295
0000000100110111


If you can find what I'm looking for, I'll hand over my old NAVICOMPUTER. Fair enough?

required: 0, placed: 441, character: 8295 0010000001100111
144 'IPUZ'
unknown1: 1, unknown2: 4, unknown3: 4294967295
0000000100110111


How about a rare and valuable BAR of DURASTEEL in exchange?

required: 0, placed: 443, character: 8306 0010000001110010
145 'IPUZ'
unknown1: 1, unknown2: 4, unknown3: 4294967295
0000000100110111


Help me out, won't you? I'll give you a  SHIELD GENERATOR for your trouble.

required: 0, placed: 444, character: 0 0000000000000000
146 'IPUZ'
unknown1: 1, unknown2: 4, unknown3: 4294967295
0000000100110111


If you ever hope to get off this planet, you'll need a DRIVE COMPENSATOR for sure. You can have mine if you help me...

required: 0, placed: 445, character: 0 0000000000000000
147 'IPUZ'
unknown1: 1, unknown2: 4, unknown3: 4294967295
0000000100110111


You're my only hope, Skywalker! My COMM UNIT is yours if you help me!

required: 0, placed: 446, character: 61440 1111000000000000
148 'IPUZ'
unknown1: 1, unknown2: 2, unknown3: 4294967295
0000000100110111


I've got an ANTI-GRAV GENERATOR with your name on it, if you can find what I need.

required: 0, placed: 447, character: 29216 0111001000100000
149 'IPUZ'
unknown1: 1, unknown2: 4, unknown3: 4294967295
0000000100110111


And get this... I have a DATA CARD filled with rare information. Very valuable, I'm sure! It's yours if you help me.

required: 0, placed: 449, character: 27936 0110110100100000
150 'IPUZ'
unknown1: 1, unknown2: 4, unknown3: 4294967295
0000000100110111


Tell you what... help me out and I'll give you a LOMMITE CRYSTAL, all the way from the mines of Elom.

required: 0, placed: 450, character: 28265 0110111001101001
151 'IPUZ'
unknown1: 1, unknown2: 4, unknown3: 4294967295
0000000100110111


If you can help me out, this sample of RYLL is yours... it's worth a bundle on the invisible market!

required: 0, placed: 452, character: 31085 0111100101101101
152 'IPUZ'
unknown1: 1, unknown2: 4, unknown3: 4294967295
0000000100110111


For my part, I'll give you a SENSOR ARRAY. Do we have a deal?

required: 0, placed: 453, character: 8274 0010000001010010
153 'IPUZ'
unknown1: 1, unknown2: 4, unknown3: 4294967295
0000000100110111


Do what you can for me, and this HYPERSPACE COMPASS is yours!

required: 0, placed: 455, character: 8311 0010000001110111
154 'IPUZ'
unknown1: 1, unknown2: 4, unknown3: 4294967295
0000000100110111


And I'll bet you could use a SENSOR PACK, am I right? It's yours if you find what I need...

required: 0, placed: 460, character: 25965 0110010101101101
155 'IPUZ'
unknown1: 0, unknown2: 4, unknown3: 4294967295
0000000100110111
Do you know what a HOLOCRON is? Me neither, but I want one!
Thanks for the Holocron. Actually, I think it's a clock!
You help me, and I'll help you... a HOLOCRON is what I'm offering. What do you say?

required: 0, placed: 461, character: 0 0000000000000000
156 'IPUZ'
unknown1: 1, unknown2: 4, unknown3: 4294967295
0000000100110111


You help me, and I'll help you... a HOLOCRON is what I'm offering. What do you say?

required: 0, placed: 461, character: 0 0000000000000000
157 'IPUZ'
unknown1: 1, unknown2: 4, unknown3: 4294967295
0000000100110111


I'll make your assistance worth while... with a top quality HOLOCUBE. Do we have a deal?

required: 0, placed: 463, character: 0 0000000000000000
158 'IPUZ'
unknown1: 1, unknown2: 4, unknown3: 4294967295
0000000100110111


Meanwhile, it looks like you've seen some space travel... do you need a TRANSPONDER by any chance? I'll trade you!

required: 0, placed: 464, character: 8289 0010000001100001
159 'IPUZ'
unknown1: 1, unknown2: 4, unknown3: 4294967295
0000000100110111


All I can offer for your assistance is a POWER CONVERTER, but I suspect you need one!

required: 0, placed: 466, character: 17184 0100001100100000
160 'IPUZ'
unknown1: 0, unknown2: 2, unknown3: 4294967295
0000000100110111
I've lost power! An ENERGY CELL, that's what I need!
Mighty kind of you to help, stranger.
For my part, I'll trade you an ENERGY CELL... fully charged!

required: 0, placed: 468, character: 0 0000000000000000
161 'IPUZ'
unknown1: 1, unknown2: 2, unknown3: 4294967295
0000000100110111


I'm not begging for a favor... I'm offering you a fully charged ENERGY CELL in exchange.

required: 0, placed: 468, character: 0 0000000000000000
162 'IPUZ'
unknown1: 1, unknown2: 4, unknown3: 4294967295
0000000100110111


I have a CRATE of  SPICE here... worth a fortune on the invisible market. It's yours if you solve my problem.

required: 0, placed: 471, character: 0 0000000000000000
163 'IPUZ'
unknown1: 1, unknown2: 4, unknown3: 4294967295
0000000100110111


Let's help each other. You've got the Force on your side, and I've got a TRAINING REMOTE to sharpen your Jedi skills.

required: 0, placed: 475, character: 0 0000000000000000
164 'IPUZ'
unknown1: 1, unknown2: 4, unknown3: 4294967295
0000000100110111


All I can offer in trade is this GLOW ROD, but you never know when you'll wind up in the dark...

required: 0, placed: 478, character: 0 0000000000000000
165 'IPUZ'
unknown1: 1, unknown2: 4, unknown3: 4294967295
0000000100110111


And you're looking for a POWER COUPLING, I hear. Let's help each other!

required: 0, placed: 479, character: 222 0000000011011110
166 'IPUZ'
unknown1: 1, unknown2: 4, unknown3: 4294967295
0000000100110111


Give me some help, and I've got a sample of CARBONITE for you. I'm talking about the real thing!

required: 0, placed: 481, character: 24864 0110000100100000
167 'IPUZ'
unknown1: 1, unknown2: 4, unknown3: 4294967295
0000000100110111


I'll trade you this old NAV CARD for some assistance. It shows this whole sector.

required: 0, placed: 484, character: 25960 0110010101101000
168 'IPUZ'
unknown1: 1, unknown2: 4, unknown3: 4294967295
0000000100110111


I don't have much to offer in return... just a BLUMFRUIT, but it's a tasty treat!

required: 0, placed: 483, character: 0 0000000000000000
169 'IPUZ'
unknown1: 0, unknown2: 4, unknown3: 4294967295
0000000100110111
Any idea where I can find a BLUMFRUIT? No one around here has even heard of them!
Mmm... this Blumfruit is delicious!
Will you help me out? I've got a BLUMFRUIT that says you will...

required: 0, placed: 483, character: 0 0000000000000000
170 'IPUZ'
unknown1: 1, unknown2: 4, unknown3: 4294967295
0000000100110111


See what you can do for me... there's a HOMING BEACON in the bargain.

required: 0, placed: 485, character: 28265 0110111001101001
171 'IPUZ'
unknown1: 1, unknown2: 4, unknown3: 4294967295
0000000100110111


If you can be of assistance in this matter, I'll trade you my father's old ELECTROSCOPE.

required: 0, placed: 487, character: 28537 0110111101111001
172 'IPUZ'
unknown1: 1, unknown2: 4, unknown3: 4294967295
0000000100110111


Help me, and you can have the RANGEFINDER off my old Bryar pistol.

required: 0, placed: 488, character: 8309 0010000001110101
173 'IPUZ'
unknown1: 1, unknown2: 4, unknown3: 4294967295
0000000100110111


A CONDENSER UNIT is what I'm offering in exchange for some help.

required: 0, placed: 489, character: 21326 0101001101001110
174 'IPUZ'
unknown1: 1, unknown2: 4, unknown3: 4294967295
0000000100110111


I must have something you might want in trade... how about a pair of BINOCULARS?

required: 0, placed: 490, character: 226 0000000011100010
175 'IPUZ'
unknown1: 0, unknown2: 4, unknown3: 4294967295
0000000100110111
Listen, do me a favor? I lost my BINOCULARS. If you come across some in your travels, remember me!
Now everything's in sight again! Thanks!
I must have something you might want in trade... how about a pair of BINOCULARS?

required: 0, placed: 490, character: 0 0000000000000000
176 'IPUZ'
unknown1: 1, unknown2: 1, unknown3: 4294967295
0000000100110111


Recently I stumbled across an IMPERIAL UTILITY BELT. Risky to keep, but I can't just give it away...

required: 0, placed: 493, character: 16928 0100001000100000
177 'IPUZ'
unknown1: 1, unknown2: 4, unknown3: 4294967295
0000000100110111


Help me out, and you can have my MACROSCOPE. I never use it any more.

required: 0, placed: 495, character: 25717 0110010001110101
178 'IPUZ'
unknown1: 1, unknown2: 4, unknown3: 4294967295
0000000100110111


I'll trade your help for my UTILITY BELT. It's got everything you need!

required: 0, placed: 496, character: 28448 0110111100100000
179 'IPUZ'
unknown1: 1, unknown2: 4, unknown3: 4294967295
0000000100110111


Now listen up... help me out, and I've got a ship's TELESPONDER for you. A JX-4, the best...

required: 0, placed: 498, character: 26740 0110100001110100
180 'IPUZ'
unknown1: 1, unknown2: 4, unknown3: 4294967295
0000000100110111


Maybe you can find a use for this HOLOCOMM. It's yours, but you've got to help me first!

required: 0, placed: 500, character: 27973 0110110101000101
181 'IPUZ'
unknown1: 1, unknown2: 4, unknown3: 4294967295
0000000100110111


In return, I've got a perfectly blank TRANSFER REGISTER for you. A smuggler might find it useful...

required: 0, placed: 501, character: 8289 0010000001100001
182 'IPUZ'
unknown1: 0, unknown2: 1, unknown3: 4294967295
0000000100110111
I need to blow something up... an IM MINE might do the trick. Get me one!
Now I'm ready to strike a blow for the Rebellion!!
If you help me, I've got an IM MINE for you... if you promise to be careful with it!

required: 0, placed: 515, character: 8304 0010000001110000
183 'IPUZ'
unknown1: 0, unknown2: 1, unknown3: 4294967295
0000000100110111
Ever heard of a Jeron FUSION CUTTER? Of course you have! Well, I need one to do some repair work...
Thanks for the Fusion Cutter! Now I can get to work!
One good turn deserves another... so I'll give you a FUSION CUTTER for your help.

required: 0, placed: 518, character: 25956 0110010101100100
184 'IPUZ'
unknown1: 0, unknown2: 1, unknown3: 4294967295
0000000100110111
Psst... confidentially, you don't know where I could get my hands on a STASIS MINE, do you?
Thanks for the mine. I've got a job for this baby!
And if you help me out, you'll be the proud new owner of a genuine STASIS MINE. A Rebel spy's dream!

required: 0, placed: 523, character: 26656 0110100000100000
185 'IPUZ'
unknown1: 3, unknown2: 4, unknown3: 4
0000000000000000

Bogus text
Retrieve the AMULET, Luke!
Welcome, Luke! Heard my call you did!

Long, long ago the Sith made an AMULET that can focus the Force for good or evil. Powerful it is... dangerous, mmm?

When the Sith fell, the amulet was lost. Now a tremor in the Force warns me... the amulet has been found on the desert world of Bakkah!

Imperial troops have it now. Soon in Vader's hands will it be, unless you find it first!

Here, take this object... small though it is, you'll need it on your quest...
required: 0, placed: 610, character: 529 0000001000010001
186 'IPUZ'
unknown1: 2, unknown2: 2, unknown3: 4294967295
0000000010000000

Nothing can stop you now!
I've got something that will help you wind up your mission... if you find me what I'm looking for.

required: 0, placed: 438, character: 30066 0111010101110010
187 'IPUZ'
unknown1: 2, unknown2: 1, unknown3: 4294967295
0000000010000000

On your way now! There's no time to lose!
You've simply got to help me! I have something that will help complete your task...

required: 0, placed: 470, character: 110 0000000001101110
188 'IPUZ'
unknown1: 2, unknown2: 1, unknown3: 4294967295
0000000010000000

Put that mine to good use!
You know, you need a big bang to finish your mission, and I've got one... a STASIS MINE! Deal with me, and it's yours!

required: 0, placed: 523, character: 0 0000000000000000
189 'IPUZ'
unknown1: 3, unknown2: 4, unknown3: 4
0000000000000000

Waldo ain't got nothing on Yoda!
I better head for Lokondo and RESCUE YODA!
Wait... I can feel a strong disturbance in the Force over here... it's some kind of message from Yoda!

What...? What...? Taken to an Imperial Base on Lokondo! Trapped in an electro-cell! Vader & the Emperor preparing to interrogate...!

I've got to RESCUE YODA before the Emperor reaches that base!

Hmm... what's this?
required: 0, placed: 1935, character: 1936 0000011110010000
190 'IPUZ'
unknown1: 2, unknown2: 2, unknown3: 4294967295
0000000010000000

Good luck. That thing should float you right to the end!
If you don't help, I won't give you this ANTI- GRAV GENERATOR, and I know you need it!

required: 0, placed: 447, character: 25974 0110010101110110
191 'IPUZ'
unknown1: 2, unknown2: 2, unknown3: 4294967295
0000000010000000

Those droid parts should come in very handy for you...
I don't have much to trade, except for a spare DROID PART I've got lying around. It's small, but vital for your work.

required: 0, placed: 465, character: 28704 0111000000100000
192 'IPUZ'
unknown1: 2, unknown2: 1, unknown3: 4294967295
0000000010000000

Now you're ready for some heavy action!
Then, if you're looking for leverage, I'll hand over the solution to all your problems... a POWER PRYBAR!

required: 0, placed: 440, character: 30496 0111011100100000
193 'IPUZ'
unknown1: 2, unknown2: 1, unknown3: 4294967295
0000000010000000

Go cleave the Empire for me...
Then we can make a trade... I've got a SONIC CLEAVER that I just know is the key to your success!

required: 0, placed: 432, character: 25964 0110010101101100
194 'IPUZ'
unknown1: 2, unknown2: 4, unknown3: 4294967295
0000000010000000

Use that Crystal wisely!
Now listen up... help me out, and I'll give you a LOMMITE CRYSTAL. It's the key to your quest!

required: 0, placed: 450, character: 28265 0110111001101001
195 'IPUZ'
unknown1: 2, unknown2: 4, unknown3: 4294967295
0000000010000000

Now get going. The Rebellion is in your hands!
I'm not asking for much... and I'll hand over a   SAMPLE OF RYLL in exchange. With it, you can fulfill your destiny!

required: 0, placed: 452, character: 31085 0111100101101101
196 'IPUZ'
unknown1: 2, unknown2: 1, unknown3: 4294967295
0000000010000000

You're near the end... finish your journey!
You've got to aid me in this matter! Why? Because I have what you need to finish your quest!

required: 0, placed: 457, character: 28265 0110111001101001
197 'IPUZ'
unknown1: 3, unknown2: 4, unknown3: 4
0000000000000000

Unused text, right>
On your own you are this time, Luke...
Weary am I, Luke...

I feel an imbalance in the Force... a CLONING MACHINE there is, survivor of the Clone Wars, guarded by adherents of the old ways, mmm? I see it, buried under a mountain of snow and ice, but where to find it, I know not!

Follow your feelings you must! Find your own way you must... but be warned: the greatest danger is YOURSELF!

Take this and leave me alone... help you further I cannot!
required: 0, placed: 1985, character: 1986 0000011111000010
198 'IPUZ'
unknown1: 3, unknown2: 4, unknown3: 4
0000000000000000


Return the LANTERN OF SACRED LIGHT to the Ewoks, Luke!
Hello again, Luke...

The day will come when our friends the Ewoks will turn the tide of battle... it is their destiny!

Now I learn they have lost their LANTERN OF SACRED LIGHT.  Without it their spirits are confused... and destiny they cannot fulfill, mmm?

Fly you must to the forest moon of Endor and find the Lantern!

Here! May this guide you....
required: 0, placed: 448, character: 1984 0000011111000000
199 'IPUZ'
unknown1: 3, unknown2: 4, unknown3: 4
0000000000000000


Find the ADEGAN CRYSTAL, Luke!
Luke! Resume your training we must, before it is too late, mmm?

Imperial troops have raided a mining operation on Halm. Why do they bother, I wonder, mmm?

They do it because they hope to find a rare ADEGAN CRYSTAL in the mine. If they take it, some young Jedi cannot his lightsaber make. Cannot then fight the Empire, no!

Find the crystal first you must! Who knows... maybe some day you yourself may have need of it, mmm?

Here! This will start you on your mission...
required: 0, placed: 459, character: 1983 0000011110111111
200 'IPUZ'
unknown1: 2, unknown2: 4, unknown3: 0
0000000010000000

You'll thank me for this!
All I can offer in trade is this GLOW ROD, but I believe you will soon find yourself in the dark...

required: 0, placed: 478, character: 0 0000000000000000
201 'IPUZ'
unknown1: 3, unknown2: 4, unknown3: 4
0000000000000000


Find THREEPIO's PARTS and put him back together!
There you are!

Once again your Rebel friends are in trouble... this time it is THREEPIO.

While operating as an undercover agent in the desert wastes of Tozeer, he was captured by Sand People. Dismantled him and scattered the parts they have!

Tread carefully! Jawas have scrounged at least one of the pieces. They are the only ones on Tozeer who can put him back together again...

Take this with you. Perhaps your quest it will help...
required: 0, placed: 1353, character: 1354 0000010101001010
202 'IPUZ'
unknown1: 2, unknown2: 4, unknown3: 3
0000000010000000

Knowledge is power! Defeat the Empire!
Help me, and I'll give you a DATA CUBE! With the information on it, you can complete your mission...

required: 0, placed: 1215, character: 0 0000000000000000
203 'IPUZ'
unknown1: 2, unknown2: 4, unknown3: 2
0000000010000000

...nor will this.
This text will never appear...

required: 0, placed: 2114, character: 109 0000000001101101
204 'IPUZ'
unknown1: 2, unknown2: 2, unknown3: 3
0000000010000000

May the Force guide your steps...
Now help me out, and I've got an ENERGY CELL for you! Just what you need to complete your task...

required: 0, placed: 468, character: 109 0000000001101101
