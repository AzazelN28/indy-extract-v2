const fs = require("fs");
const yargs = require("yargs");
const Canvas = require("canvas");
const Reader = require("./Reader");
const leftPad = require("./leftPad");

const Game = {
  INDIANA_JONES: 0x01,
  YODA_STORIES: 0x02
};

const ActionTriggerNames = new Map([
  [0x00, "on_first_enter"],
  [0x01, "on_enter"],
  [0x02, "on_bump_tile"],
  [0x03, "on_drag_item"],
  [0x04, "on_walk"],
  [0x05, "on_temp_var_eq"],
  [0x06, "on_rand_var_eq"],
  [0x07, "on_rand_var_gt"],
  [0x08, "on_rand_var_lt"],
  [0x09, "on_enter_vehicle"],
  [0x0A, "on_check_map_tile"],
  [0x0B, "on_enemy_dead"],
  [0x0C, "on_all_enemies_dead"],
  [0x0D, "on_has_item"],
  [0x0E, "on_check_end_item"],
  [0x0F, "on_check_start_item"],
  [0x10, "on_pick_item"],
  [0x11, "on_game_in_progress"],
  [0x12, "on_game_complete"],
  [0x13, "on_health_lt"],
  [0x14, "on_health_gt"],
  [0x15, "on_unknown"],
  [0x16, "on_unknown2"],
  [0x17, "on_drag_wrong_item"],
  [0x18, "on_player_at_pos"],
  [0x19, "on_global_var_eq"],
  [0x1A, "on_global_var_lt"],
  [0x1B, "on_global_var_gt"],
  [0x1C, "on_experience_eq"],
  [0x1D, "on_unknown3"],
  [0x1E, "on_unknown4"],
  [0x1F, "on_temp_var_neq"],
  [0x20, "on_rand_var_neq"],
  [0x21, "on_global_var_neq"],
  [0x22, "on_check_map_tile_var"],
  [0x23, "on_experience_gt"]
]);

const ActionTriggers = {
  ON_FIRST_ENTER: 0x00,
  ON_ENTER: 0x01,
  ON_BUMP_TILE: 0x02,
  ON_DRAG_ITEM: 0x03,
  ON_WALK: 0x04,
  ON_TEMP_VAR_EQ: 0x05,
  ON_RAND_VAR_EQ: 0x06,
  ON_RAND_VAR_GT: 0x07,
  ON_RAND_VAR_LT: 0x08,
  ON_ENTER_VEHICLE: 0x09,
  ON_CHECK_MAP_TILE: 0x0A,
  ON_ENEMY_DEAD: 0x0B,
  ON_ALL_ENEMIES_DEAD: 0x0C,
  ON_HAS_ITEM: 0x0D,
  ON_CHECK_END_ITEM: 0x0E,
  ON_CHECK_START_ITEM: 0x0F,
  ON_PICK_ITEM: 0x10,
  ON_GAME_IN_PROGRESS: 0x11,
  ON_GAME_COMPLETE: 0x12,
  ON_HEALTH_LT: 0x13,
  ON_HEALTH_GT: 0x14,
  UNKNOWN: 0x15,
  UNKNOWN2: 0x16,
  ON_DRAG_WRONG_ITEM: 0x17,
  ON_PLAYER_AT_POS: 0x18,
  ON_GLOBAL_VAR_EQ: 0x19,
  ON_GLOBAL_VAR_LT: 0x1A,
  ON_GLOBAL_VAR_GT: 0x1B,
  ON_EXPERIENCE_EQ: 0x1C,
  UNKNOWN3: 0x1D,
  UNKNOWN4: 0x1E,
  ON_TEMP_VAR_NEQ: 0x1F,
  ON_RAND_VAR_NEQ: 0x20,
  ON_GLOBAL_VAR_NEQ: 0x21,
  ON_CHECK_MAP_TILE_VAR: 0x22,
  ON_EXPERIENCE_GT: 0x23
};

const ActionCommandNames = new Map([
  [0x00, "set_tile"],
  [0x01, "clear_tile"],
  [0x02, "move_map_tile"],
  [0x03, "draw_overlay_tile"],
  [0x04, "say_text"],
  [0x05, "show_text"],
  [0x06, "redraw_tile"],
  [0x07, "redraw_tiles"],
  [0x08, "render_changes"],
  [0x09, "wait_ticks"],
  [0x0A, "play_sound"],
  [0x0B, "transition_in"],
  [0x0C, "random"],
  [0x0D, "set_temp_var"],
  [0x0E, "add_temp_var"],
  [0x0F, "set_map_tile_var"],
  [0x10, "release_camera"],
  [0x11, "lock_camera"],
  [0x12, "set_player_pos"],
  [0x13, "move_camera"],
  [0x14, "flag_once"],
  [0x15, "show_object"],
  [0x16, "hide_object"],
  [0x17, "show_entity"],
  [0x18, "hide_entity"],
  [0x19, "show_all_entities"],
  [0x1A, "hide_all_entities"],
  [0x1B, "spawn_item"],
  [0x1C, "add_item_to_inv"],
  [0x1D, "remove_item_from_inv"],
  [0x1E, "open"],
  [0x1F, "unknown"],
  [0x20, "unknown2"],
  [0x21, "warp_to_map"],
  [0x22, "set_global_var"],
  [0x23, "add_global_var"],
  [0x24, "set_rand_var"],
  [0x25, "add_health"]
]);

const ActionCommands = {
  SET_TILE: 0x00,
  CLEAR_TILE: 0x01,
  MOVE_MAP_TILE: 0x02,
  DRAW_OVERLAY_TILE: 0x03,
  SAY_TEXT: 0x04,
  SHOW_TEXT: 0x05,
  REDRAW_TILE: 0x06,
  REDRAW_TILES: 0x07,
  RENDER_CHANGES: 0x08,
  WAIT_TICKS: 0x09,
  PLAY_SOUND: 0x0A,
  TRANSITION_IN: 0x0B,
  RANDOM: 0x0C,
  SET_TEMP_VAR: 0x0D,
  ADD_TEMP_VAR: 0x0E,
  SET_MAP_TILE_VAR: 0x0F,
  RELEASE_CAMERA: 0x10,
  LOCK_CAMERA: 0x11,
  SET_PLAYER_POS: 0x12,
  MOVE_CAMERA: 0x13,
  FLAG_ONCE: 0x14,
  SHOW_OBJECT: 0x15,
  HIDE_OBJECT: 0x16,
  SHOW_ENTITY: 0x17,
  HIDE_ENTITY: 0x18,
  SHOW_ALL_ENTITIES: 0x19,
  HIDE_ALL_ENTITIES: 0x1A,
  SPAWN_ITEM: 0x1B,
  ADD_ITEM_TO_INV: 0x1C,
  REMOVE_ITEM_FROM_INV: 0x1D,
  OPEN: 0x1E,
  UNKNOWN: 0x1F,
  UNKNOWN2: 0x20,
  WARP_TO_MAP: 0x21,
  SET_GLOBAL_VAR: 0x22,
  ADD_GLOBAL_VAR: 0x23,
  SET_RAND_VAR: 0x24,
  ADD_HEALTH: 0x25
};

const CharacterFlags = {
  FRIENDLY: 0x1,
  ENEMY: 0x2,
  IS_WEAPON: 0x4,

  BEHAVIOR_HARD: 0x10000,
  BEHAVIOR_MEDIUM: 0x20000,
  BEHAVIOR_RANDOM: 0x30000,
  BEHAVIOR_STATIONARY: 0x40000,
  BEHAVIOR_ANIMATED: 0x80000
};

const CharacterDirection = {
  UP: 0,
  DOWN: 1,
  UP_LEFT: 2,
  LEFT: 3,
  DOWN_LEFT: 4,
  UP_RIGHT: 5,
  RIGHT: 6,
  DOWN_RIGHT: 7
};

const CharacterFrame = {

  DOWN: 1,
  UP: 2,
  UP_LEFT: 2,
  LEFT: 3,
  DOWN_LEFT: 4,
  UP_RIGHT: 5,
  RIGHT: 6,
  DOWN_RIGHT: 7,

  //Sprites for hero, walking
  WALK_UP_1: 8,
  WALK_DOWN_1: 9,
  WALK_UP_LEFT_1: 10,
  WALK_LEFT_1: 11,
  WALK_DOWN_LEFT_1: 12,
  WALK_UP_RIGHT_1: 13,
  WALK_RIGHT_1: 14,
  WALK_DOWN_RIGHT_1: 15,
  WALK_UP_2: 16,
  WALK_DOWN_2: 17,
  WALK_UP_LEFT_2: 18,
  WALK_LEFT_2: 19,
  WALK_DOWN_LEFT_2: 20,
  WALK_UP_RIGHT_2: 21,
  WALK_RIGHT_2: 22,
  WALK_DOWN_RIGHT_2: 23,

  //Sprites for weapon, attacking.
  //Extend is used for whips (Indianna Jones) and Sabers (Luke)
  WEAPON_PROJECTILE_UP: 0,
  WEAPON_PROJECTILE_DOWN: 1,
  WEAPON_PROJECTILE_UP_LEFT: 2,
  WEAPON_PROJECTILE_LEFT: 3,
  WEAPON_PROJECTILE_DOWN_LEFT: 4,
  WEAPON_PROJECTILE_UP_RIGHT: 5,
  WEAPON_PROJECTILE_RIGHT: 6,

  WEAPON_ICON: 7,
  ATTACK_UP_1: 8,
  ATTACK_DOWN_1: 9,
  ATTACK_EXTEND_UP_2: 10,
  ATTACK_LEFT_1: 11,
  ATTACK_EXTEND_DOWN_1: 12,
  ATTACK_EXTEND_LEFT_1: 13,
  ATTACK_RIGHT_1: 14,
  ATTACK_EXTEND_RIGHT_1: 15,
  ATTACK_UP_2: 16,
  ATTACK_DOWN_2: 17,
  ATTACK_EXTEND_UP_1: 18,
  ATTACK_LEFT_2: 19,
  ATTACK_EXTEND_DOWN_2: 20,
  ATTACK_EXTEND_LEFT_2: 21,
  ATTACK_RIGHT_2: 22,
  ATTACK_EXTEND_RIGHT_2: 23

};

const TileType = {
  GAME_OBJECT     : 0x00000001,
  NON_COLLIDING   : 0x00000002,
  COLLIDING       : 0x00000004,
  PUSH_PULL_BLOCK : 0x00000008,
  MINIMAP         : 0x00000010,
  WEAPON          : 0x00000020,
  ITEM            : 0x00000040,
  CHARACTER       : 0x00000080
};

const TileWeapon = {
  LIGHT_BLASTER   : 0x00001000,
  HEAVY_BLASTER   : 0x00002000,
  LIGHTSABER      : 0x00004000,
  THE_FORCE       : 0x00008000
};

const TileItem = {
  KEYCARD          : 0x00001000,
  PUZZLE_ITEM      : 0x00002000,
  PUZZLE_RARE_ITEM : 0x00004000,
  PUZZLE_KEY_ITEM  : 0x00008000,
  LOCATOR          : 0x00010000,
  MEDIKIT          : 0x00020000
};

const TileMinimap = {
  DOOR                : 0x00001000,
  HOME                : 0x00002000,
  PUZZLE_UNSOLVED     : 0x00004000,
  PUZZLE_SOLVED       : 0x00008000,
  GATEWAY_UNSOLVED    : 0x00010000,
  GATEWAY_SOLVED      : 0x00020000,
  UP_WALL_LOCKED      : 0x00040000,
  DOWN_WALL_LOCKED    : 0x00080000,
  LEFT_WALL_LOCKED    : 0x00100000,
  RIGHT_WALL_LOCKED   : 0x00200000,
  UP_WALL_UNLOCKED    : 0x00400000,
  DOWN_WALL_UNLOCKED  : 0x00800000,
  LEFT_WALL_UNLOCKED  : 0x01000000,
  RIGHT_WALL_UNLOCKED : 0x02000000,
  OBJECTIVE           : 0x04000000
};

const IZON = {
  TRIGGER_LOCATION       : 0x00,
  SPAWN_LOCATION         : 0x01,
  FORCE_RELATED_LOCATION : 0x02,
  VEHICLE_TO_SEC_MAP     : 0x03,
  VEHICLE_TO_PRI_MAP     : 0x04,
  OBJECT_LOCATOR         : 0x05,
  CRATE_ITEM             : 0x06,
  PUZZLE_NPC             : 0x07,
  CRATE_WEAPON           : 0x08,
  DOOR_IN                : 0x09,
  DOOR_OUT               : 0x0A,
  UNUSED                 : 0x0B,
  LOCK                   : 0x0C,
  TELEPORTER             : 0x0D,
  XWING_FROM_DAGOBAH     : 0x0E,
  XWING_TO_DAGOBAH       : 0x0F
};

// @see https://github.com/shinyquagsire23/DesktopAdventures/blob/master/src/include/map.h
const ZoneFlags = {
  ENEMY_TERRITORY: 1,                   // 112 de estos.
  FINAL_DESTINATION: 2,                 // Hay 6 de estos (2 por cada tipo de planeta).
  ITEM_FOR_ITEM: 3,                     // Hay 6 de estos (2 por cada tipo de planeta).
  FIND_SOMETHING_USEFUL_NPC: 4,         // Hay 6 de estos (2 por cada tipo de planeta).
  ITEM_TO_PASS: 5,                      // Hay otros 6 de estos (2 por cada tipo de planeta).
  FROM_ANOTHER_MAP: 6,                  // De estos hay 12 + 1 Dagobah.
  TO_ANOTHER_MAP: 7,                    // De estos hay 12.
  INDOORS: 8,                           // De estos hay 264.
  INTRO_SCREEN: 9,                      // Sólo hay uno (el mapa 0)
  FINAL_ITEM: 10,                       // De estos hay 14
  MAP_START_AREA: 11,                   // De estos hay 3 (uno por cada tipo de planeta y son: 2, 151 y el 476).
  UNUSED_C: 12,                         // NADA
  VICTORY_SCREEN: 13,                   // 76
  LOSS_SCREEN: 14,                      // 77
  MAP_TO_ITEM_FOR_LOCK: 15,             // De estos hay 68
  FIND_SOMETHING_USEFUL_DROP: 16,       // Lugares donde aparecen objetos.
  FIND_SOMETHING_USEFUL_BUILDING: 17,   // Lugares donde aparecen objetos.
  FIND_THE_FORCE: 18                    // Hay 6 de estos (2 por cada tipo de planeta)
};

const ZoneType = {
  DESERT : 0x01,
  SNOW   : 0x02,
  FOREST : 0x03,
  SWAMP  : 0x05
};

/**
 * @see https://github.com/shinyquagsire23/DesktopAdventures/issues/6
 * if ( world_size_param == WORLD_SMALL )
 * {
 *     world_width_max = 8;
 *     world_width_min = 5;
 *     world_unk1_max = 6;
 *     world_unk1_min = 4;
 *     world_unk2_max = 1;
 *     world_unk2_min = 1;
 *     unk3_max = 1;
 *     unk3_min = 1;
 * }
 * else if ( world_size_param == WORLD_LARGE )
 * {
 *     world_width_max = 12;
 *     world_width_min = 6;
 *     world_unk1_max = 12;
 *     world_unk1_min = 6;
 *     world_unk2_max = 11;
 *     world_unk2_min = 6;
 *     unk3_max = 11;
 *     unk3_min = 4;
 * }
 * else
 * {
 *     world_width_max = 9;
 *     world_width_min = 5;
 *     world_unk1_max = 9;
 *     world_unk1_min = 5;
 *     world_unk2_max = 8;
 *     world_unk2_min = 4;
 *     unk3_max = 8;
 *     unk3_min = 3;
 * }
 *
 * Looking at the last screenshot, it looks like there's at least a
 * minimum of two item exchanges. I might keep setting values to
 * minimums to get an idea of the minimum requirements for worlds.
 */

const WorldSize = {
  SMALL: 0x01,
  MEDIUM: 0x02,
  LARGE: 0x03
};

const stats = new Map();

const argv = yargs
  .usage("Usage: $0 --data <file>")
  .example("$0 -d desktop.daw")
  .count("verbose")
  .alias("v","verbose")
  .alias("q","quiet")
  .alias("d","data")
  .nargs("d", 1)
  .describe("d", "Data file (YODESK.DTA or DESKTOP.DAW)")
  .demandOption(["d"])
  .argv;

const buffer = fs.readFileSync(argv.data);

const tiles = [];

const game = (argv.data.toLowerCase() === "yodesk.dta") ? Game.YODA_STORIES : Game.INDIANA_JONES;
const gameName = (game === Game.YODA_STORIES) ? "yoda" : "indy";

const palette = require(`./palettes/${gameName}`);

const reader = new Reader(buffer);
while (!reader.isComplete) {
  const id4 = reader.readID4();
  console.log(id4);
  let size, count;
  switch(id4) {
    // VERSION
    case "VERS":
      const version = reader.readUint32();
      console.log(`Version: ${version.toString(16)}`);
      break;

    // ZONE DATA
    case "ZONE":
      if (game === Game.INDIANA_JONES) {

        size = reader.readUint32(true);
        reader.push(size);
        count = reader.readUint16(true);
        console.log(`Zones: ${count}`);
        let index = 0;
        while (!reader.isComplete) {
          const subid4 = reader.readID4();
          console.log(subid4);
          if (subid4 !== "IZON") {
            throw new Error(`Wrong id, expected IZON, found ${subid4}`);
          }
          const subsize = reader.readUint32(true); // this size includes the IZON part.
          const width = reader.readUint16(true);
          const height = reader.readUint16(true);
          const unknown = reader.readUint32(true);
          {
            let canvas, context;
            if (!argv.quiet) {
              canvas = new Canvas(width * 32, height * 32);
              context = canvas.getContext("2d");
            }
            for (let y = 0; y < height; y++) {
              for (let x = 0; x < width; x++) {
                const lo = reader.readUint16(true);
                const mi = reader.readUint16(true);
                const hi = reader.readUint16(true);
                if (!argv.quiet) {
                  if (lo >= 0 && lo < tiles.length) {
                    if (!tiles[lo]) {
                      console.error(`No existe ${lo} en tiles`);
                    }
                    context.drawImage(tiles[lo], x * 32, y * 32);
                  }
                  if (mi >= 0 && mi < tiles.length) {
                    if (!tiles[mi]) {
                      console.error(`No existe ${lo} en tiles`);
                    }
                    context.drawImage(tiles[mi], x * 32, y * 32);
                  }
                  if (hi >= 0 && hi < tiles.length) {
                    if (!tiles[hi]) {
                      console.error(`No existe ${lo} en tiles`);
                    }
                    context.drawImage(tiles[hi], x * 32, y * 32);
                  }
                }
              }
            }

            if (!argv.quiet) {
              const buffer = canvas.toBuffer(undefined, 9, canvas.PNG_FILTER_NONE);
              console.log(`Writing zones/${gameName}/${leftPad(index, 4)}.png`);
              fs.writeFileSync(`./zones/${gameName}/${leftPad(index, 4)}.png`, buffer);
            }
          }
          index++;
        }
        reader.pop();

      } else if (game === Game.YODA_STORIES) {

        count = reader.readUint16(true);
        for (let index = 0; index < count; index++) {
          const zoneType = reader.readUint16(true);
          const zoneLength = reader.readUint32(true);
          //reader.skip(zoneLength);
          //continue; // TODO: Arreglar el código de abajo.
          const zoneId = reader.readUint16(true);
          /**
           * IZON
           */
          const izon = reader.readID4();
          if (izon !== "IZON") {
            throw new Error(`Wrong id, expected IZON, found ${izon}`);
          }
          const izonSize = reader.readUint32(true);
          const width = reader.readUint16(true);
          const height = reader.readUint16(true);
          const type = reader.readUint32(true);

          // Este campo es SIEMPRE 0xFFFF así que me imagino que no hay NADA
          // ÚTIL.
          const izonUnknown = reader.readUint16(true);
          const world = reader.readUint16(true);
          const tilemap = [[]];
          {
            let canvas, context;
            if (!argv.quiet) {
              canvas = new Canvas(width * 32, height * 32);
              context = canvas.getContext("2d");
            }
            for (let y = 0; y < height; y++) {
              for (let x = 0; x < width; x++) {
                const lo = reader.readUint16(true);
                const mi = reader.readUint16(true);
                const hi = reader.readUint16(true);
                if (!argv.quiet) {
                  if (lo >= 0 && lo < tiles.length) {
                    if (!tiles[lo]) {
                      console.error(`No existe ${lo} en tiles`);
                    }
                    context.drawImage(tiles[lo], x * 32, y * 32);
                  }
                  if (mi >= 0 && mi < tiles.length) {
                    if (!tiles[mi]) {
                      console.error(`No existe ${lo} en tiles`);
                    }
                    context.drawImage(tiles[mi], x * 32, y * 32);
                  }
                  if (hi >= 0 && hi < tiles.length) {
                    if (!tiles[hi]) {
                      console.error(`No existe ${lo} en tiles`);
                    }
                    context.drawImage(tiles[hi], x * 32, y * 32);
                  }
                }
                tilemap[y][x] = [lo,mi,hi];
              }
              tilemap.push([]);
            }
            if (!argv.quiet) {
              const buffer = canvas.toBuffer(undefined, 9, canvas.PNG_FILTER_NONE);
              console.log(`Writing zones/${gameName}/${leftPad(zoneId, 4)}.png`);
              fs.writeFileSync(`./zones/${gameName}/${leftPad(zoneId, 4)}.png`, buffer);
              fs.appendFileSync(`./zones/${gameName}/zones.csv`,
                `"${zoneId}","${width}","${height}","${type}","${world}"\n`
              );
            }
          }

          console.log(zoneId, izon, width, height, type, izonUnknown, world);

          // WTF? ESTO TIENE PINTA DE QUE TIENE QUE VER CON LOS
          // HOTSPOTS.
          const objectInfoCount = reader.readUint16(true);
          const objects = [];
          for (let subindex = 0; subindex < objectInfoCount; subindex++) {
            const objectType = reader.readUint32(true);
            const objectX = reader.readUint16(true);
            const objectY = reader.readUint16(true);
            const objectUnknown = reader.readUint16(true);
            const objectParam = reader.readUint16(true);
            console.log(objectType,objectX,objectY,objectUnknown,objectParam);
            objects.push({
              type: objectType,
              x: objectX,
              y: objectY,
              unknown: objectUnknown,
              param: objectParam
            });
          }

          /**
           * IZAX
           */
          const izax = reader.readID4();
          if (izax !== "IZAX") {
            throw new Error(`Wrong id, expected IZAX, found ${izax}`);
          }
          const izaxSize = reader.readUint32(true) - 8;
          const izaxUnknown = reader.readUint16(true);
          const charCount = reader.readUint16(true);
          for (let subindex = 0; subindex < charCount; subindex++) {
            const charId = reader.readUint16(true);
            const x = reader.readUint16(true);
            const y = reader.readUint16(true);
            const item = reader.readUint16(true);
            const numItems = reader.readUint32(true);
            for (let i = 0; i < 16; i++) {
              reader.readUint16(true);
            }
          }
          const tile1Count = reader.readUint16(true);
          for (let subindex = 0; subindex < tile1Count; subindex++) {
            reader.readUint16(true);
          }
          const tile2Count = reader.readUint16(true);
          for (let subindex = 0; subindex < tile2Count; subindex++) {
            reader.readUint16(true);
          }

          /**
           * IZX2
           */
          const izx2 = reader.readID4();
          if (izx2 !== "IZX2") {
            throw new Error(`Wrong id, expected IZX2, found ${izx2}`);
          }
          const izx2Size = reader.readUint32(true) - 8;
          const izx2Count = reader.readUint16(true);
          for (let subindex = 0; subindex < izx2Count; subindex++) {
            reader.readUint16(true);
          }

          /**
           * IZX3
           */
          const izx3 = reader.readID4();
          if (izx3 !== "IZX3") {
            throw new Error(`Wrong id, expected IZX3, found ${izx3}`);
          }
          const izx3Size = reader.readUint32(true) - 8;
          const izx3Count = reader.readUint16(true);
          for (let subindex = 0; subindex < izx3Count; subindex++) {
            reader.readUint16(true);
          }

          /**
           * IZX4
           */
          const izx4 = reader.readID4();
          if (izx4 !== "IZX4") {
            throw new Error(`Wrong id, expected IZX4, found ${izx4}`);
          }
          const izx4Size = reader.readUint32(true) - 8;
          const izx4Unknown = reader.readUint16(true);

          /**
           * IACT
           */
          const actionCount = reader.readUint16(true);
          const actions = [];
          for (let subindex = 0; subindex < actionCount; subindex++) {
            const triggers = [];
            const commands = [];

            const iact = reader.readID4();
            if (iact !== "IACT") {
              throw new Error(`Wrong id, expected IACT, found ${iact}`);
            }
            const iactSize = reader.readUint32(true);
            const triggerCount = reader.readUint16(true);
            console.log(iact, subindex);
            console.log("Triggers:", triggerCount);
            for (let index = 0; index < triggerCount; index++) {
              const triggerCommand = reader.readUint16(true);
              const arg1 = reader.readUint16(true);
              const arg2 = reader.readUint16(true);
              const arg3 = reader.readUint16(true);
              const arg4 = reader.readUint16(true);
              const arg5 = reader.readUint16(true);
              const arg6 = reader.readUint16(true);
              if (ActionTriggerNames.has(triggerCommand)) {
                console.log("\t", ActionTriggerNames.get(triggerCommand), arg1, arg2, arg3, arg4, arg5, arg6);
                triggers.push([ActionTriggerNames.get(triggerCommand), arg1,arg2,arg3,arg4,arg5,arg6]);
              } else {
                console.log("Unknown trigger command");
              }
            }


            const scriptCommandCount = reader.readUint16(true);
            console.log("Commands:", scriptCommandCount);
            for (let index = 0; index < scriptCommandCount; index++) {
              const command = reader.readUint16(true);
              const arg1 = reader.readUint16(true);
              const arg2 = reader.readUint16(true);
              const arg3 = reader.readUint16(true);
              const arg4 = reader.readUint16(true);
              const arg5 = reader.readUint16(true);
              const arg6 = reader.readUint16(true);
              if (arg6 > 0) {
                console.log(reader.readString(arg6));
              }
              if (ActionCommandNames.has(command)) {
                console.log("\t",ActionCommandNames.get(command), arg1, arg2, arg3, arg4, arg5, arg6);
                commands.push([ActionCommandNames.get(command), arg1,arg2,arg3,arg4,arg5,arg6]);
              } else {
                console.log("Unknown command");
              }
            }

            actions.push({
              triggers,
              commands
            })
          }

          if (!argv.quiet) {
            fs.writeFileSync(`./zones/${gameName}/${leftPad(zoneId, 4)}.json`, JSON.stringify({
              zoneId,
              zoneType,
              width,
              height,
              type,
              world,
              objects,
              actions,
              tilemap
            }, null, "  "));
          }
        }
      }
      break;

    // SOUND DATA
    case "SNDS":
      size = reader.readUint32(true);
      reader.push(size);
      reader.skip(2);
      while (!reader.isComplete) {
        const stringSize = reader.readUint16(true);
        console.log(reader.readString(stringSize), stringSize);
      }
      reader.pop();
      break;

    // TILE DATA
    case "TILE":
      let index = 0;
      size = reader.readUint32(true);
      reader.push(size);
      while (!reader.isComplete) {
        const flags = reader.readUint32(true);
        if (argv.quiet) {
          reader.skip(1024);
        } else {
          /*
          const bs = leftPad(bullshit.toString(2), 32);
          if (stats.has(bs)) {
            stats.set(bs, stats.get(bs) + 1);
          } else {
            stats.set(bs, 1);
          }
          */
          const canvas = new Canvas(32, 32);
          const context = canvas.getContext("2d");
          context.antialias = "none";
          for (let y = 0; y < 32; y++) {
            for (let x = 0; x < 32; x++) {
              const color = reader.readUint8();
              const offset = color * 4;
              const b = palette[offset + 0];
              const g = palette[offset + 1];
              const r = palette[offset + 2];
              if (color === 0) {
                context.fillStyle = `rgba(${r},${g},${b},0.0)`;
              } else {
                context.fillStyle = `rgba(${r},${g},${b},1.0)`;
              }
              context.fillRect(x,y,1,1);
            }
          }
          //tiles.push(reader.readBytes(1024));
          const buffer = canvas.toBuffer(undefined, 9, canvas.PNG_FILTER_NONE);
          tiles.push(canvas);
          console.log(`Writing tiles/${gameName}/${leftPad(index, 4)}.png`);
          fs.writeFileSync(`./tiles/${gameName}/${leftPad(index, 4)}.png`, buffer);

          /*const stream = fs.createWriteStream(`./tiles/${gameName}/${leftPad(index, 4)}.png`)
          canvas.pngStream()
                .on("error", (err) => {
                  console.error(err);
                })
                .on("data", (chunk) => {
                  stream.write(chunk);
                })
                .on("finish", () => {
                  stream.end();
                  console.log(`Writing tiles/${gameName}/${leftPad(index, 4)}.png`);
                });*/

        }
        index++;
      }
      reader.pop();
      break;

    // SETUP SCREEN
    case "STUP":
      size = reader.readUint32(true);
      if (argv.quiet) {
        reader.skip(size);
      } else {
        const canvas = new Canvas(288,288);
        const context = canvas.getContext("2d");
        for (let y = 0; y < 288; y++) {
          for (let x = 0; x < 288; x++) {
            const color = reader.readUint8();
            const offset = color * 4;
            const b = palette[offset + 0];
            const g = palette[offset + 1];
            const r = palette[offset + 2];
            if (color === 0) {
              context.fillStyle = `rgba(${r},${g},${b},0.0)`;
            } else {
              context.fillStyle = `rgba(${r},${g},${b},1.0)`;
            }
            context.fillRect(x,y,1,1);
          }
        }
        const buffer = canvas.toBuffer(undefined, 9, canvas.PNG_FILTER_NONE);
        console.log(`Writing title/${gameName}.png`);
        fs.writeFileSync(`./title/${gameName}.png`, buffer);
        //canvas.pngStream().pipe(fs.createWriteStream(`./title/${gameName}.png`));
      }
      //const image = reader.readBytes(size);
      break;

    // ESTOS OBJETOS SÓLO EXISTEN
    // EN INDIANA_JONES
    // ZONE AUX, AUX2 AND AUX3
    case "ZAUX":
    case "ZAX2":
    case "ZAX3":
      size = reader.readUint32(true);
      reader.push(size);
      while (!reader.isComplete) {
        const subid4 = reader.readID4();
        console.log(subid4);
        if (subid4 !== "IZAX" && subid4 !== "IZX2" && subid4 !== "IZX3" && subid4 !== "IZX4") {
          throw new Error(`Wrong id, expected IZAX, found ${subid4}`);
        }
        const subsize = reader.readUint32(true);
        reader.skip(subsize - 8);
      }
      reader.pop();
      break;

    // SÓLO INDIANA JONES
    // ZONE AUX 4
    case "ZAX4":
      size = reader.readUint32(true);
      reader.push(size);
      while (!reader.isComplete) {
        const subid4 = reader.readID4();
        console.log(subid4);
        if (subid4 !== "IZX4") {
          throw new Error(`Wrong id, expected IZX4, found ${subid4}`);
        }
        reader.skip(6);
      }
      reader.pop();
      break;

    // PUZZLE DATA
    case "PUZ2":
      if (game === Game.INDIANA_JONES) {

        //---------------------------------------------------
        // INDIANA JONES
        //---------------------------------------------------
        size = reader.readUint32(true);
        reader.push(size);
        while (!reader.isComplete) {
          const index = reader.readUint16(true);
          if (index === 0xFFFF) {
            break;
          }
          const subid4 = reader.readID4();
          console.log(index, subid4);
          if (subid4 !== "IPUZ") {
            throw new Error(`Wrong id, expected IPUZ, found ${subid4}`);
          }
          const subsize = reader.readUint32(true);
          const puzzleType = reader.readUint32(true);
          const unknown2 = reader.readUint32(true);
          const unknown3 = reader.readUint16(true);
          // Cadena 1
          //  0 => 54
          //  26 > x > 114 ()
          // Cadena 2
          //  0 => 50
          // Cadena 3
          //  26 > x >
          // Cadena 4
          //  0 => 134
          //  478 > x > 194 (15 => posibles finales?)
          for (let j = 0; j < 4; j++) {
            const stringLength = reader.readUint16(true);
            console.log(reader.readString(stringLength));

            /*if (stats.has(`IPUZ.strings.${j}`)) {
              const stringsMap = stats.get(`IPUZ.strings.${j}`);
              if (stringsMap.has(stringLength)) {
                stringsMap.set(stringLength, stringsMap.get(stringLength) + 1);
              } else {
                stringsMap.set(stringLength, 1);
              }
            } else {
              stats.set(`IPUZ.strings.${j}`, new Map());
            }*/
          }
          const unknown4 = reader.readUint16(true);
          const tile1 = reader.readUint16(true);
        }
        reader.pop();

      } else {

        //---------------------------------------------------
        // YODA STORIES
        //---------------------------------------------------
        size = reader.readUint32(true);
        reader.push(size);
        while (!reader.isComplete) {
          const index = reader.readUint16(true);
          if (index === 0xFFFF) {
            break;
          }
          const subid4 = reader.readID4();
          console.log(index, subid4);
          if (subid4 !== "IPUZ") {
            throw new Error(`Wrong id, expected IPUZ, found ${subid4}`);
          }
          const subsize = reader.readUint32(true);
          // Map { 0 => 73, 1 => 74, 2 => 43, 3 => 15 }
          // Esto debe de almacenar algún valor indicando el tipo
          // de puzzle o algo así (¿Quizá indica el world?)
          const puzzleType = reader.readUint32(true);
          // Map { 4 => 105, 2 => 35, 1 => 50, 0 => 15 }
          // Es posible que haya alguna relación entre estos números
          // y el número total de inicios del juego.
          // 105 / 35 = 3
          // 105 / 15 = 7
          const unknown2 = reader.readUint32(true);
          // Map { 0 => 86, 4 => 15, 4294967295 => 101, 3 => 2, 2 => 1 }
          // Más flags?
          const unknown3 = reader.readUint32(true);
          console.log(`puzzleType: ${puzzleType}, unknown2: ${unknown2}, unknown3: ${unknown3}`);

          // Esto claramente no va a ser un zoneId
          const unknown4 = reader.readUint16(true);
          const flags = leftPad(unknown4.toString(2), 16);
          console.log(flags);
          if (stats.has(flags)) {
            stats.set(flags, stats.get(flags) + 1);
          } else {
            stats.set(flags, 1);
          }
          // Cadena 1
          //  0 => 127
          //  26 > x > 114 ()
          // Cadena 2
          //  0 => 77
          // Cadena 3
          //  26 =>
          // Cadena 4
          //  0 => 189
          //  478 > x > 194 (15 => posibles finales?)
          const strings = [];
          for (let j = 0; j < 4; j++) {
            const stringLength = reader.readUint16(true);
            const string = reader.readString(stringLength);
            strings.push(string);
            console.log(string);

            /*
            if (stats.has(`IPUZ.strings.${j}`)) {
              const stringsMap = stats.get(`IPUZ.strings.${j}`);
              if (stringsMap.has(stringLength)) {
                stringsMap.set(stringLength, stringsMap.get(stringLength) + 1);
              } else {
                stringsMap.set(stringLength, 1);
              }
            } else {
              stats.set(`IPUZ.strings.${j}`, new Map());
            }
            */
          }
          // @see https://tcrf.net/Star_Wars:_Yoda_Stories_(Windows)
          // Esto creo que puede tener que ver con esta pantalla.
          const unusedLength = reader.readUint16(true);
          const firstItem = reader.readUint16(true);
          const secondItem = reader.readUint16(true);
          console.log(`firstItem: ${firstItem}, secondItem: ${secondItem} ${leftPad(secondItem.toString(2), 16)}`);

          if (!argv.quiet) {
            fs.appendFileSync(`./puzzles/${gameName}/puzzles.csv`,
              `"${index}","${puzzleType}","${unknown2}","${unknown3}","${unknown4}","${flags}","${strings[0]}","${strings[1]}","${strings[2]}","${strings[3]}","${firstItem}","${secondItem}"\n`
            );

            fs.writeFileSync(`./puzzles/${gameName}/${leftPad(index,4)}.json`, JSON.stringify({
              puzzleType,
              unknown2,
              unknown3,
              unknown4,
              flags,
              strings,
              unusedLength,
              firstItem,
              secondItem
            }, null, "  "));
          }

        }
        reader.pop();
      }
      break;

    // CHARACTER DATA
    case "CHAR":
      if (game === Game.INDIANA_JONES) {

        size = reader.readUint32(true);
        reader.push(size);
        while(!reader.isComplete) {
          const index = reader.readUint16(true);
          if (index === 0xFFFF) {
            break;
          }
          const subid4 = reader.readID4();
          if (subid4 !== "ICHA") {
            throw new Error(`Wrong id, expected ICHA, found ${subid4}`);
          }
          const subsize = reader.readUint32(true);
          const name = reader.readString(0x10);
          const flags = reader.readUint32(true);
          console.log(index, name.length, name, flags);
          const tiles = [];
          for (let f = 0; f < 0x30; f+= 0x02) {
            tiles.push(reader.readUint16(true));
          }
          //reader.skip(0x30);
        }
        reader.pop();

      } else if (game === Game.YODA_STORIES) {

        size = reader.readUint32(true);
        reader.push(size);
        while(!reader.isComplete) {
          const index = reader.readUint16(true);
          if (index === 0xFFFF) {
            break;
          }
          const subid4 = reader.readID4();
          if (subid4 !== "ICHA") {
            throw new Error(`Wrong id, expected ICHA, found ${subid4}`);
          }
          const subsize = reader.readUint32(true);
          const name = reader.readString(0x10);
          const flags1 = reader.readUint16(true);
          const flags2 = reader.readUint16(true);
          console.log(index, name, name.length, flags1, flags2);
          const tiles = [];
          for (let subindex = 0; subindex < 0x36; subindex += 0x02) {
            tiles.push(reader.readUint16(true));
          }
          //reader.skip(0x36);
        }
        reader.pop();

      }
      break;

    // TILE NAME AND PUZZLE NAME
    case "TNAM":
      if (game === Game.INDIANA_JONES) {

        size = reader.readUint32(true);
        reader.push(size);
        while (!reader.isComplete) {
          const tileId = reader.readUint16(true);
          if (tileId === 0xFFFF) {
            break;
          }
          const name = reader.readString(0x10);
          console.log(tileId, name);
        }
        reader.pop();

      } else if (game === Game.YODA_STORIES) {

        size = reader.readUint32(true);
        reader.push(size);
        while (!reader.isComplete) {
          const tileId = reader.readUint16(true);
          if (tileId === 0xFFFF) {
            break;
          }
          const name = reader.readString(0x18);
          console.log(tileId, name);
        }
        reader.pop();

      }
      break;

    case "ZNAM":
      size = reader.readUint32(true);
      reader.push(size);
      while (!reader.isComplete) {
        const tileId = reader.readUint16(true);
        if (tileId === 0xFFFF) {
          break;
        }
        const name = reader.readString(0x10);
        console.log(tileId, name);
      }
      reader.pop();
      break;

    // PUZZLE NAME
    case "PNAM":
      size = reader.readUint32(true);
      reader.push(size);
      const unknown = reader.readUint16(true);
      while (!reader.isComplete) {
        const name = reader.readString(0x10);
        console.log(name);
      }
      reader.pop();
      break;

    // ACTION NAME
    case "ANAM":
      size = reader.readUint32(true);
      reader.push(size);
      while (!reader.isComplete) {
        const puzzleId = reader.readUint16(true); //??
        if (puzzleId === 0xFFFF) {
          break;
        }
        // tileId vale de 0 a 44 (casi de forma incremental).
        // 0 => 1
        // 1 => 303
        // 2 => 278
        // 3 => 253
        // 4 => 226
        // 5 => 190
        // 6 => 162
        // 7 => 145
        // 8 => 122
        // 0xFFFF => 319
        // 9 => 102
        // 10 => 91
        // 11 => 82
        // 12 => 70
        // 13 => 64
        // 14 => 55
        // 15 => 51
        // 16 => 47
        // 17 => 42
        // 18 => 38
        // 19 => 35
        // 20 => 31
        // 21 => 25
        // 22 => 20
        // 23 => 14
        // 24 => 12
        // 25 => 7
        // 26 => 5
        // 27 => 4
        // 32 => 3
        // 33 => 2
        // 34 => 1
        const tileId = reader.readUint16(true);
        if (tileId === 0xFFFF) {
          // ESTO ES EL IDENTIFICADOR DE LA ZONA.
          const zoneId = reader.readUint16(true);
          if (zoneId === 0xFFFF) {
            break;
          } else {
            // ESTO NO ES NADA, PADDING DE MIERDA, VALE 0 318 veces.
            const nothing = reader.readUint16(true);
          }
        }
        const name = reader.readString(0xE);
        console.log(tileId, name);
      }
      reader.pop();
      break;

    // CHARACTER WEAPON
    case "CHWP":
      size = reader.readUint32(true);
      reader.push(size);
      while (!reader.isComplete) {
        // índice del personaje.
        const index = reader.readUint16(true);
        if (index === 0xFFFF) {
          break;
        }
        // Arma que suelta cuando muere. En este caso
        // tendría sentido que para el jugador fuera 0xFFFF
        // porque eso indicaría que no suelta ninguna.
        //
        // NOP, esto casi seguro que es algún tipo de flag.
        //
        // 0xFFFF => 10
        // 0x0900 => 1
        // 0x0A00 => 1
        // 0xFF00 => 2
        // 0x0200 => 8
        // 0x0B00 => 3
        // 0x0C00 => 1
        // 0x0D00 => 1
        // ESTO ES OTRO ID?¿?¿?¿
        const dropsWeapon = reader.readUint16(true);

        // SEGÚN https://github.com/shinyquagsire23/DesktopAdventures/blob/master/src/include/character.h
        // ESTO SERÍA LA SALUD.
        // N.P.I (+ flags?)
        // 0x0000 => 4
        // 0x0A00 => 4
        // 0x0200 => 3
        // 0x0300 => 2
        // 0x0400 => 3
        // 0x0800 => 3
        // 0x0C00 => 1
        // 0x0100 => 4
        // 0x3200 => 1
        // 0x6300 => 2
        const health = reader.readUint16(true);
      }
      reader.pop();
      break;

    // CHARACTER AUX
    case "CAUX":
      size = reader.readUint32(true);
      reader.push(size);
      while (!reader.isComplete) {
        const index = reader.readUint16(true);
        if (index === 0xFFFF) {
          break;
        }
        // SEGÚN https://github.com/shinyquagsire23/DesktopAdventures/blob/master/src/include/character.h
        // ESTO ES EL DAÑO QUE INFLIGE.
        // N.P.I (+ flags?)
        // 0x0100 => 2
        // 0x0200 => 1
        // 0x0500 => 4
        // 0x1900 => 1
        // 0x1400 => 3
        // 0x0300 => 2
        // 0x0400 => 5
        // 0x0600 => 4
        // 0x0A00 => 2
        // 0x0700 => 1
        // 0x2100 => 1
        // 0x3200 => 1
        const unknown = reader.readUint16(true);
        console.log(index, unknown, unknown.toString(16));
      }
      reader.pop();
      break;

    // ACTIONS
    case "ACTN":
      size = reader.readUint32(true);
      reader.push(size);
      while (!reader.isComplete) {
        // ESTE VALOR ES ÚNICO Y CASI SEGURO QUE IDENTIFICA
        // A UNA ZONA PORQUE HAY 366.
        const group = reader.readUint16(true);
        if (group === 0xFFFF) {
          break;
        }
        const actionCount = reader.readUint16(true);
        for (let index = 0; index < actionCount; index++) {
          const subid4 = reader.readID4();
          if (subid4 !== "IACT") {
            throw new Error(`Wrong id, expected IACT, found ${subid4}`);
          }
          // ESTO SIEMPRE ES 0x00000000 (2825 veces).
          const unknown = reader.readUint32(true);
          const triggerCount = reader.readUint16(true);
          console.log(subid4, index, unknown);
          console.log("Triggers:", triggerCount);
          for (let index = 0; index < triggerCount; index++) {
            const triggerCommand = reader.readUint16(true);
            const arg1 = reader.readUint16(true);
            const arg2 = reader.readUint16(true);
            const arg3 = reader.readUint16(true);
            const arg4 = reader.readUint16(true);
            const arg5 = reader.readUint16(true);
            const arg6 = reader.readUint16(true);
            if (ActionTriggerNames.has(triggerCommand)) {
              console.log("\t", ActionTriggerNames.get(triggerCommand), arg1, arg2, arg3, arg4, arg5, arg6);
            } else {
              console.log("Unknown trigger command");
            }
          }
          const scriptCommandCount = reader.readUint16(true);
          console.log("Commands:", scriptCommandCount);
          for (let index = 0; index < scriptCommandCount; index++) {
            const command = reader.readUint16(true);
            const arg1 = reader.readUint16(true);
            const arg2 = reader.readUint16(true);
            const arg3 = reader.readUint16(true);
            const arg4 = reader.readUint16(true);
            const arg5 = reader.readUint16(true);
            const arg6 = reader.readUint16(true);
            if (arg6 > 0) {
              console.log(reader.readString(arg6));
            }
            if (ActionCommandNames.has(command)) {
              console.log("\t",ActionCommandNames.get(command), arg1, arg2, arg3, arg4, arg5, arg6);
            } else {
              console.log("Unknown command");
            }
          }
        }
      }
      reader.pop();
      break;

    // HOTSPOT
    case "HTSP":
      size = reader.readUint32(true);
      reader.push(size);
      while (!reader.isComplete) {
        const zoneId = reader.readUint16(true);
        if (zoneId === 0xFFFF) {
          break;
        }
        const count = reader.readUint16(true);
        for (let i = 0; i < count; i++) {
          const type = reader.readUint32(true);
          const x = reader.readUint16(true);
          const y = reader.readUint16(true);
          const unknown = reader.readUint16(true);
          const param = reader.readUint16(true);
          console.log("HotSpot", type, x, y, unknown, param);
        }
      }
      reader.pop();
      break;

    // END OF FILE
    case "ENDF":
      size = reader.readUint32(true);
      reader.seek(size);
      break;

    default:
      throw new Error(`Invalid ID ${id4}`);
  }
}

console.dir(stats);
/*console.log(JSON.stringify(data, null, "  "));*/
//if (!argv.quiet) {
//fs.writeFileSync("./data.json", JSON.stringify(data, null, "  "));
//}
