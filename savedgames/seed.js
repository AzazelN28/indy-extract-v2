const fs = require("fs");

let currentSeed, lastSeed;
for (let i = 2; i < process.argv.length; i++) {
  const name = process.argv[i];
  const buffer = fs.readFileSync(name);
  currentSeed = buffer.readUInt32LE(9);
  if (i > 2) {
    console.log(lastSeed !== currentSeed);
  }
  lastSeed = currentSeed;
}
