
const a = parseInt(process.argv[2], 10);
const b = parseInt(process.argv[3], 10);
const expectedSize = 0xd4;

let size = 0;

/*
size += 4 + 4;
for (let index = 0; index < b; index++) {
  size += 2 + 2 + 4 + 2 + 2;
}
*/

if (a != 0) {
  size += 4;

  for (let index = 0; index < a; index++) {
    size += 2 + 2 + 2 + 2 + 4
          + 2 + 2 + 2 + 2 + 4
          + 4 + 4
          + 2 + 2 + 2 + 2 + 2 + 2
          + 4 + 4 + 4 + 2 + 2;
    for (let j = 0; j < 4; j++) {
      size += 4 + 4;
    }
  }

  size += 4;

  if (b > 0) {
    for (let index = 0; index < b; index++) {
      size += 4;
    }
  }
}

deviatedSize = expectedSize - size;

console.log(`${size} 0x${size.toString(16)}, ${expectedSize} 0x${expectedSize.toString(16)}, ${deviatedSize} 0x${deviatedSize.toString(16)}`);
