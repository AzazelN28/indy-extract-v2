const fs = require("fs");

const files = [
  "savegame01.wld",
  "savegame02.wld",
  "savegame03.wld",
  "savegame04.wld",
  "savegame05.wld",
];

let offset = parseInt(process.argv[2], 10);

const length = process.argv[3] ? parseInt(process.argv[3], 10) : 1;
const rows = process.argv[4] ? parseInt(process.argv[4], 10) : 1;

const buffers = files.map((file) => fs.readFileSync(file));

for (let index = 0; index < rows; index++) {
  const values = buffers.map((buffer, subindex) => {
    const suboffset = process.argv[5 + subindex] ? parseInt(process.argv[5 + subindex], 10) : 0;
    if (length === 1) {
      return buffer.readUInt8(offset + suboffset);
    } else if (length === 2) {
      return buffer.readUInt16LE(offset + suboffset);
    }
    return buffer.readUInt32LE(offset + suboffset);
  });
  console.log(offset.toString().padStart(5, '0'), values.map((value) => `${value.toString(16).padStart(4, "0")} ${value.toString(10).padStart(5)}`).join(" "));
  offset += length;
}
