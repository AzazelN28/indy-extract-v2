const fs = require("fs");
const path = require("path");

const files = fs.readdirSync(process.cwd()).filter((file) => path.extname(file) === ".sav");
const last = [];
for (const file of files) {
  const buffer = fs.readFileSync(file);
  last.push(buffer.slice(-0x40));
}

const map = new Map();
for (let ia = 0; ia < last.length - 1; ia++) {
  const a = last[ia];
  for (let ib = ia + 1; ib < last.length; ib++) {
    const b = last[ib];
    for (let j = 0; j < 0x40; j++) {
      if (a[j] !== b[j]) {
        if (!map.has(j)) {
          map.set(j, 0);
        }
        map.set(j, map.get(j) + 1);
        const k = map.get(j);
        process.stdout.write(`\x1B[${k};${j * 2}H${a[j]}`);
      }
    }
  }
}
