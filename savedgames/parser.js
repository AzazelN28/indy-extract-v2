const fs = require("fs");
const path = require("path");
const Canvas = require("canvas");

const MapType = [
  "NOP", // 0
  "ENEMY_TERRITORY", // 1
  "FINAL_DESTINATION", // 2
  "ITEM_FOR_ITEM", // 3
  "FIND_SOMETHING_USEFUL_NPC", // 4
  "ITEM_TO_PASS", // 5
  "FROM_ANOTHER_MAP", // 6
  "TO_ANOTHER_MAP", // 7
  "INDOORS", // 8
  "INTRO_SCREEN", // 9
  "FINAL_ITEM", // A
  "MAP_START_AREA", // B
  "UNUSED_C", // C
  "VICTORY_SCREEN", // D
  "LOSS_SCREEN", // E
  "MAP_TO_ITEM_FOR_LOCK", // F
  "FIND_SOMETHING_USEFUL_DROP", // 10
  "FIND_SOMETHING_USEFUL_BUILDING", // 11
  "FIND_THE_FORCE" // 12
];

const ObjectTypeName = [
  "QUEST_ITEM_SPOT", // 0
  "SPAWN", // 1
  "THE_FORCE", // 2
  "VEHICLE_TO", // 3
  "VEHICLE_FROM", // 4
  "LOCATOR", // 5
  "ITEM", // 6
  "PUZZLE_NPC", // 7
  "WEAPON", // 8
  "DOOR_IN", // 9
  "DOOR_OUT", // 10 (A)
  "UNKNOWN", // 11
  "LOCK", // 12
  "TELEPORTER", // 13
  "XWING_FROM", // 14
  "XWING_TO" // 15
];

const ObjectType = {
  QUEST_ITEM_SPOT: 0,
  SPAWN: 1,
  THE_FORCE: 2,
  VEHICLE_TO: 3,
  VEHICLE_FROM: 4,
  LOCATOR: 5,
  ITEM: 6,
  PUZZLE_NPC: 7,
  WEAPON: 8,
  DOOR_IN: 9,
  DOOR_OUT: 10,
  UNKNOWN: 11,
  LOCK: 12,
  TELEPORTER: 13,
  XWING_FROM: 14,
  XWING_TO: 15
};

const tiles = fs.readdirSync(path.resolve(__dirname,
   "../tiles/yoda"))
  .filter((file) => path.extname(file) === ".png")
  .map((file) => path.resolve(__dirname, `../tiles/yoda/${file}`))
  .map((file) => fs.readFileSync(file))
  .map((buffer) => {
    const image = new Canvas.Image();
    image.src = buffer;
    return image;
  });

class ReadBuffer {
  static fromFile(file) {
    const readFile = util.promisify(fs.readFile);
    return readFile(file).then((buffer) => {
      return new ReadBuffer(buffer);
    });
  }

  static from(buffer) {
    return new ReadBuffer(buffer);
  }

  constructor(buffer, endianess = true, offset = 0) {
    this._buffer = buffer;
    this._endianess = endianess;
    this._offset = offset;
  }

  get availableBytes() {
    return this._buffer.byteLength - this._offset;
  }

  readString(length) {
    const value = this._buffer.slice(this._offset, this._offset + length).toString("utf8");
    this._offset += length;
    return value;
  }

  readU8() {
    const value = this._buffer.readUInt8(this._offset);
    this._offset ++;
    return value;
  }

  readU16() {
    const value = this._endianess ? this._buffer.readUInt16LE(this._offset) : this._buffer.readUInt16BE(this._offset);
    this._offset += 2;
    return value;
  }

  readU32() {
    const value = this._endianess ? this._buffer.readUInt32LE(this._offset) : this._buffer.readUInt32BE(this._offset);
    this._offset += 4;
    return value;
  }

  readS8() {
    const value = this._buffer.readInt8(this._offset);
    this._offset ++;
    return value;
  }

  readS16() {
    const value = this._endianess ? this._buffer.readInt16LE(this._offset) : this._buffer.readInt16BE(this._offset);
    this._offset += 2;
    return value;
  }

  readS32() {
    const value = this._endianess ? this._buffer.readInt32LE(this._offset) : this._buffer.readInt32BE(this._offset);
    this._offset += 4;
    return value;
  }

  offset(value) {
    this._offset = value;
    return this;
  }

  skip(length) {
    this._offset += length;
    return this;
  }
}

const buffer = ReadBuffer.from(fs.readFileSync(process.argv[2]));

function readZoneMetadata() {
  return {
    visited: buffer.readU32(),  // 00 04
    u02: buffer.readU32(),  // 04 08

    u03: buffer.readU32(),  // 08 12
    u04: buffer.readU32(),  // 12 16
    u05: buffer.readU32(),  // 16 20

    zoneId: buffer.readU16(),  // 20 22
    u07: buffer.readU16(),  // 22 24
    u08: buffer.readU16(),  // 24 26
    item: buffer.readU16(),  // 26 28
    u10: buffer.readU16(),  // 28 30
    u11: buffer.readU16(),  // 30 32
    u12: buffer.readU16(),  // 32 34
    character: buffer.readU16(),  // 34 36

    wtf: buffer.readU32(),  // 38 40

    u15: buffer.readU16(),  // 40 42
  };
}

function readZoneData() {
  // LoadGame_00427f42
  // Creo que esto es el zone Id
  const zoneId = buffer.readU16();
  console.log("zoneId", zoneId);
  const zone = JSON.parse(fs.readFileSync(path.resolve(__dirname, "..", "zones", "yoda", `${zoneId.toString().padStart(4, 0)}.json`)).toString("utf8"));
  const { width: zoneWidth, height: zoneHeight } = zone;
  // De esto no tengo ni puta idea.
  const zoneWasVisited = buffer.readU32();
  console.log("zoneWasVisited:", zoneWasVisited);

  // zoneWasVisited > 0?
  if (zoneWasVisited !== 0) {
    console.log("reading bullshit");
    const zoneStructure = {
      u01: buffer.readU32(),
      u02: buffer.readU32(),
      u03: buffer.readU32(),
      u04: buffer.readU32(),
      u05: buffer.readU16(),
      wtf: buffer.readU16()
    };
    console.log("------------------------ IMPORTANT!");
    console.log(zoneStructure);
    console.log("------------------------ IMPORTANT!");

    const canvas = new Canvas(32 * zoneWidth, 32 * zoneHeight);
    const context = canvas.getContext("2d");
    for (let ty = 0; ty < zoneHeight; ty++) {
      for (let tx = 0; tx < zoneWidth; tx++) {
        const tb = buffer.readU16();
        const tm = buffer.readU16();
        const tt = buffer.readU16();

        if (tb !== 0xFFFF) {
          context.drawImage(tiles[tb], tx * 32, ty * 32);
        }
        if (tm !== 0xFFFF) {
          context.drawImage(tiles[tm], tx * 32, ty * 32);
        }
        if (tt !== 0xFFFF) {
          context.drawImage(tiles[tt], tx * 32, ty * 32);
        }
        context.strokeStyle = "#f00";
        context.strokeRect(tx * 32, ty * 32, 32, 32);

        process.stdout.write(tm !== 0xFFFF ? "#" : " ");
      }
      process.stdout.write("\n");
    }
    fs.writeFileSync(`${path.basename(process.argv[2], ".wld")}_${zoneId}.png`, canvas.toBuffer());
  }

  const zoneStructure2 = {
    u01: buffer.readU32(),
    u02: buffer.readU32()
  };
  console.log(zoneStructure2);

  // Esto no está bien, no tengo muy claro lo que significa
  // ¿tal vez indique si se debe cargar o no?
  const EntityType = {
    ZONE: 0,
    TILE: 1
  };

  const zoneThings = [];
  for (let i = 0; i < zoneStructure2.u02; i++) {
    const unknownStructure = {
      enabled: buffer.readU16(),
      id: buffer.readU16(),
      type: buffer.readU32(),
      x: buffer.readU16(), // a esto hay que sumarle 1
      y: buffer.readU16() // a esto hay que sumarle 1
    };
    unknownStructure.typeName = ObjectTypeName[unknownStructure.type];
    console.log("-----------[ ENTITY ]-----------");
    console.log(unknownStructure);
    console.log("--------------------------------");
    zoneThings.push(unknownStructure);
  }

  const a = (process.argv[3] && parseInt(process.argv[3], 10)) || zoneStructure2.u01 > 0;
  const b = (process.argv[4] && parseInt(process.argv[4], 10)) || zoneStructure2.u02 > 0;
  if (a && zoneWasVisited) {
    const uxx = buffer.readU32();
    console.log(uxx);
    for (let i = 0; i < a; i++) {
      // Hay D0 bytes que ni puta idea de lo que son.
      const superStructure = {
        u01: buffer.readU16(),
        u02: buffer.readU16(),
        u03: buffer.readU16(),
        u04: buffer.readU16(),
        u05: buffer.readU32(),
        u06: buffer.readU16(),
        u07: buffer.readU16(),
        u08: buffer.readU16(),
        u09: buffer.readU16(),
        u10: buffer.readU32(),
        u11: buffer.readU32(),
        u12: buffer.readU32(),
        u13: buffer.readU16(),
        u14: buffer.readU16(),
        u15: buffer.readU16(),
        u16: buffer.readU16(),
        u17: buffer.readU16(),
        u18: buffer.readU16(),
        u19: buffer.readU32(),
        u20: buffer.readU32(),
        u21: buffer.readU32(),
        u22: buffer.readU16(),
        u23: buffer.readU16()
      };
      console.log(superStructure);

      // En SaveGame_004077cc, hace lo siguiente:
      //  mov ebp, 04h
      // así que deduzco que debe haber 4 registros.
      for (let j = 0; j < 4; j++) {
        console.log(
          buffer.readU32(),
          buffer.readU32()
        );
      }
      // Power Coupling
      debugger;
    }

    // 4
    const uyy = buffer.readU32();
    console.log(uyy);
    // 4 * 4 =
    for (let i = 0; i < b; i++) {
      buffer.readU32();
    }
  }

  for (const zoneThing of zoneThings) {
    if (zoneThing.type === ObjectType.DOOR_IN) {
      console.log("reading subzone");
      readZoneData();
    }
  }
}

function readZone() {
  // LoadGame_00427f13
  const zoneX = buffer.readU32();
  const zoneY = buffer.readU32();
  console.log(zoneX, zoneY);
  if (zoneX !== 0xffffffff
   && zoneY !== 0xffffffff) {
    readZoneData();
    return true;
  } else {
    console.log("skip zone");
    return false;
  }
}

function readInventory() {
  const length = buffer.readU32();
  const inventory = [];
  for (let index = 0; index < length; index++) {
    inventory.push(buffer.readU16());
  }
  console.log("Inventory", inventory);
  return inventory;
}

function readPlayerData() {
  return {
    zoneId: buffer.readU16(),
    zoneX: buffer.readU32(),
    zoneY: buffer.readU32(),

    u01: buffer.readU16(),
    u02: buffer.readU16(), // picked?
    u03: buffer.readU16(),
    u04: buffer.readU16(), // picked?

    u05: buffer.readU32(),
    u06: buffer.readU32(),
    u07: buffer.readU32(),
    u08: buffer.readU32(),
    u09: buffer.readU32(),
    u10: buffer.readU32(),
    u11: buffer.readU32(),
    u12: buffer.readU32(),
    u13: buffer.readU32(),
    u14: buffer.readU32()
  };
}

// LoadGame_LoadHeader_00427bc2 [label="LoadGame_LoadHeader_00427bc2\n9"]
// --------------------------------------------------------------------
const header = buffer.readString(9);
if (header !== "YODASAV44") {
  throw new Error("Invalid header");
}

// LoadGame_BasicData_00427c64
// --------------------------------------------------------------------
const seed = buffer.readU32();
const world = buffer.readU32();
const size = buffer.readU32(); // tamaño?

console.log({
  seed,
  world,
  size
});

let length, index, lengthX, lengthY;

{
  length = buffer.readU16();
  index = 0;
  do {
    const x = buffer.readU16();
    console.log(x);
  } while (++index < length);
}

{
  length = buffer.readU16();
  index = 0;
  do {
    const x = buffer.readU16();
    console.log(x);
  } while (++index < length);
}

const yodaZonesMetadata = [
  [null,null],
  [null,null]
];

const gameZonesMetadata = [
  [null, null, null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null, null, null],
  [null, null, null, null, null, null, null, null, null, null]
];

lengthY = 2;
for (let y = 0; y < lengthY; y++) {
  lengthX = 2;
  for (let x = 0; x < lengthX; x++) {
    const yodaZoneMetadata = readZoneMetadata();
    console.log("------------------------------------------------")
    console.log(yodaZoneMetadata);
    console.log("------------------------------------------------")
    yodaZonesMetadata[y][x] = yodaZoneMetadata;
  }
}

//readZone();
for (let y = 0; y < lengthY; y++) {
  for (let y = 0; y < lengthX; y++) {
    readZone();
  } // for (let x = 0; x < lengthX; x++);
} // for (let y = 0; y < lengthY; y++);

const test = {
  u01: buffer.readU32(),
  u02: buffer.readU32()
}
console.log(test);
console.log("offset: ", buffer._offset - 8, (buffer._offset - 8).toString(16), "0xFFFFFFFF: ", test.u01 === 0xFFFFFFFF);
console.log("offset: ", buffer._offset - 4, (buffer._offset - 4).toString(16), "0xFFFFFFFF: ", test.u02 === 0xFFFFFFFF);

lengthY = 10;
lengthX = 10;
for (let y = 0; y < lengthY; y++) {
  for (let x = 0; x < lengthX; x++) {
    const gameZoneMetadata = readZoneMetadata();
    console.log(x, y, gameZoneMetadata);
    gameZonesMetadata[y][x] = gameZoneMetadata;
  }
}

for (const row of gameZonesMetadata) {
  for (const gameZoneMetadata of row) {
    if (gameZoneMetadata.zoneId !== 0xFFFF) {
      process.stdout.write(`${gameZoneMetadata.zoneId.toString().padStart(3, 0)} `)
    } else {
      process.stdout.write("### ");
    }
  }
  process.stdout.write("\n");
}

// FIXME: Esto se podría resolver con un simple return
// si esto estuviera dentro de una función (que lo estará).
let isComplete = false;
for (let y = 0; y < lengthY; y++) {
  for (let y = 0; y < lengthX; y++) {
    if (!readZone()) {
      isComplete = true;
      break;
    }
  } // for (let x = 0; x < lengthX; x++);
  if (isComplete) {
    break;
  }
} // for (let y = 0; y < lengthY; y++);

console.log(readInventory());
console.log(readPlayerData());
