const fs = require("fs");
const buffer = fs.readFileSync(process.argv[2]);
const value = parseInt(process.argv[3], 10);
const contextSize = process.argv[4] ? parseInt(process.argv[4], 10) : 16;

function showContext(buffer, offset, size, length) {
  const halfSize = size * 0.5;
  const start = Math.max(0, offset - halfSize);
  const end = Math.min(buffer.byteLength, offset + halfSize);
  const subbuffer = buffer.slice(start, end);
  let str = "", color = "";
  for (let i = 0; i < subbuffer.byteLength; i++) {
    const j = offset - start;
    if (i >= j && i <= j + length - 1) {
      color = "\x1B[0;32m";
    } else {
      color = "\x1B[0m";
    }
    str += ` ${color}${(subbuffer[i]).toString(16).padStart(2, "0")}`;
  }
  console.log(str);
}

if (value > 255) {
  for (let i = 0; i < buffer.byteLength - 1; i++) {
    if (buffer.readUInt16BE(i) === value) {
      console.log(`Found at ${i} byte (0x${i.toString(16)}) (big)`);
      showContext(buffer,i,contextSize,2);
    } else if (buffer.readUInt16LE(i) === value) {
      console.log(`Found at ${i} byte (0x${i.toString(16)}) (little)`);
      showContext(buffer,i,contextSize,2);
    }
  }
} else {
  for (let i = 0; i < buffer.byteLength; i++) {
    if (buffer[i] === value) {
      console.log(`Found at ${i} byte (0x${i.toString(16)})`);
      showContext(buffer,i,contextSize,1);
    }
  }
}
