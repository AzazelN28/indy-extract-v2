const fs = require("fs");
const path = require("path");

const buffer = fs.readFileSync(process.argv[2]);
const outputBuffer = buffer.slice(9);

fs.writeFileSync(`${path.basename(process.argv[2], ".wld")}.sav`, outputBuffer);
