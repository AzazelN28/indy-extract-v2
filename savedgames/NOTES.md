# Partidas guardadas

Zonas indoor

  Desde     A

  151       263
  151       152
  151       264
  336       339
  336       340
  336       338
  336       341

Mapa extraído usando Ctrl+Shift+F8 y Yoda Stories

```
    ### ### ### ### ??? ### ### ### ### ###
    ### ### ### ### 390 ### ### ### ### ###
    ### ### 188 098 225 174 262 191 240 164
    ### ### ### 640 168 171 260 329 161 ###
    336 652 226 242 638 151 224 465 ### ###
    ### 177 ### 172 ### 097 197 ### ### ###
    ### ### ### 251 ### ### 183 137 204 293
    ### ### ### ??? ### ### 162 484 179 233
    ### ### ### ### ### ### 645 232 134 ###
```

Mapa real (Extraido usando reader.js)

```
    153 ### ### ### 132 ### ### ### ### ###
    222 ### ### ### 485 ### ### ### ### ###
    466 ### ### ### 390 ### ### ### ### ###
    ### ### 188 098 225 174 262 191 240 164
    ### ### ### 640 168 171 260 329 161 ###
    336 652 226 242 638 151 224 465 ### ###
    ### 177 ### 172 ### 097 197 ### ### ###
    ### ### ### 251 ### ### 183 137 204 293
    ### ### ### 259 ### ### 162 484 179 233
    ### ### ### ### ### ### 645 232 134 ###
```

Zonas

  Parece que la primera zona está en 02F6D y va hasta 2F94 y está guardado
  en forma de array 2D.

  390
  188
  098
  225
  174
  262
  191
  240
  164
  640
  168
  171
  260
  329
  161
  336
  652
  226
  242
  638
  151
  224
  465
  177
  172
  097
  197
  251
  183
  137
  204
  293
  162
  484
  179
  233
  645
  232
  134

¿Qué podría estar guardado en los archivos estos?
- Valor de las variables globales.
- Valor de la semilla o semillas elegidas para generar el escenario.
- Es posible que las zonas puedan tener variables asociadas.
- Las zonas, sus coordenadas, cuáles han sido visitadas y cuáles no, (y quizá cuántas veces se ha pasado por una zona).
- Inventario del jugador
- Salud del jugador
- Casi con total seguridad NO SE GUARDA LA VELOCIDAD DE JUEGO pero sí LA DIFICULTAD DEL COMBATE.

Terrain Locator

- Circulo: Spaceport (Mapa inicial)
- Puzzle:
- Bloqueos: Un bloqueo es un tipo de puzzle que te impide
pasar a determinadas areas del juego hasta que encuentres una
solución.
- Gateways: A veces verás áreas del juego que están aisladas
del resto del mundo. No puedes alcanzarlas hasta que encuentres
una conexión especial.
- Teleport Stations: Repartidos por el mapa hay dispositivos
místicos que permiten a Luke saltar rápidamente de un área a
otra. Busca plataformas de piedra con extraños glifos grabados
en ella. Algunas de estas plataformas están activas y otras hay
que activarlas resolviendo puzzles. Una vez que Luke tiene el
mapa en su posesión, saltando en los glifos mostrarán el mapa
con cada una de las posiciones a las que puedes saltar activas.
- Final chapter: Este símbolo representa el área donde será
el "final showdown".

Inventario
- Lightsaber (18)
- R2D2 (794)
- Power Bryar (440)
- Thermal Detonator (514)
- Ice Mushroom (1196)

Usando ctrl+shift+f8

savegame01.wld  - z:  94, x:  7, y:  4, e: 108
savegame01a.wld - z:  94, x:  7, y:  5, e: 108
savegame01b.wld - z:  94, x:  8, y:  3, e: 108
savegame01c.wld - z:  93, x:  7, y: 12, e: 108
savegame01d.wld - z: 151, x:  8, y: 11, e: 108
savegame01e.wld - z: 151, x:  5, y: 12, e: 108 (thermal detonator)
savegame01f.wld - z: 151, x:  2, y: 15, e: 108 (ice mushroom + thermal detonator)
savegame01g.wld - z: 264, x:  6, y:  3, e: 108 (**-**)
savegame01h.wld - z: 151, x:  6, y: 15, e: 108 (**-**) - velocidad máxima
savegame01i.wld - z: 151, x:  6, y: 15, e: 108 (**-**) - velocidad mínima
savegame01j.wld - z: 151, x:  6, y: 15, e: 108 (**-**) - dificultad máxima
savegame01k.wld - z: 151, x:  6, y: 15, e: 108 (**-**) - dificultad mínima
savegame01l.wld - z:  97, x:  6, y:  0, e: 108

Al parecer en savegame01h,i,j y k
la posición del jugador se encuentra
al final de todo el archivo.

Tenemos que en:
offset 0x62EB se encuentra el valor 0x9700 => 151
que es el sector en el que se encuentra el jugador
después tenemos 4 bytes con los valores 05000000
y 05000000 después 2 bytes con FFFF después

0x62EB 2 bytes 97 00
0x62ED 4 bytes 05 00 00 00
0x62F1 4 bytes 05 00 00 00
0x62F5 2 bytes FF FF
0x62F7 4 bytes 00 00 00 00
0x62FB 2 bytes 00 00
0x62FD 4 bytes C0 00 00 00
0x6301 2 bytes E0 01 (coordenadas)
0x6303 2 bytes 00 00
0x6305 4 bytes 01 00 00 00
0x6309 4 bytes 01 00 00 00
0x630D 4 bytes 32 00 00 00
0x6311 4 bytes 00 00 00 00
0x6315 4 bytes 0E 00 00 00
0x6319 4 bytes 00 00 00 00
0x631D 4 bytes 6C 00 00 00 (108)
0x6321 4 bytes 6C 00 00 00 (108)

El inventario en savegame01.sav se encuentra en la dirección 0x51E6,
ahí hay un valor de 32 bits que indica la cantidad de items que se
tienen y acto seguido, la lista de los items (16 bits)

# Descripción del formato

# A grandes rasgos

Cabecera
Zonas
Mapa
Jugador

---------------- Cabecera ----------------
YODASAV99                   - 9 bytes
Semilla aleatoria           - 4 bytes
Tipo de mundo               - 4 bytes
?                           - 4 bytes

num. bloque 1               - 2 bytes
                            - 2 bytes * num. bloque 1
num. bloque 2               - 2 bytes
                            - 2 bytes * num. bloque 2

