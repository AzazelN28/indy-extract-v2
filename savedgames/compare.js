const fs = require("fs");

const a = fs.readFileSync(process.argv[2]);
const b = fs.readFileSync(process.argv[3]);
const output = process.argv[4] || 10;

function getOutputPadding(output) {
  switch (output) {
    case 2: return 8;
    case 8: return 4;
    case 10: return 3;
    case 16: return 2;
    default: return 0;
  }
}

const outputPadding = getOutputPadding(output);

const byteLength = Math.min(a.byteLength, b.byteLength);
for (let i = 0; i < byteLength; i++) {
  if (a[i] !== b[i]) {
    console.log(
      i,
      a[i].toString(output).padStart(outputPadding, '0'),
      b[i].toString(output).padStart(outputPadding, '0')
    );
  }
}
