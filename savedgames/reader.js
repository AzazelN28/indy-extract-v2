const fs = require("fs");

const buffer = fs.readFileSync(process.argv[2]);

function getWorldName(value) {
  switch (value) {
    case 1: return "Desierto";
    case 2: return "Hielo";
    case 3: return "Bosque";
    default: return "Desconocido";
  }
}

//---------------------------------------------------------
// Cabecera
//---------------------------------------------------------
const header = buffer.slice(0,9).toString("utf8");
if (header !== "YODASAV44") {
  console.error("Invalid header");
}

//---------------------------------------------------------
// Semilla
//---------------------------------------------------------
const seed = buffer.readUInt32LE(9);
console.log("Semilla: ", seed);

//---------------------------------------------------------
// Tipo de mundo.
//---------------------------------------------------------
const world = buffer.readUInt32LE(13);
console.log("Tipo de mundo: ", world,  getWorldName(world));

//---------------------------------------------------------
// Unknown
//---------------------------------------------------------
const u1 = buffer.readUInt32LE(17);

let offset = 21;

//---------------------------------------------------------
//
//---------------------------------------------------------
const u2 = buffer.readUInt16LE(offset);
for (let i = 0; i < u2; i++) {
  offset += 2;
  const u2u = buffer.readUInt16LE(offset);
  console.log(u2u);
}
offset += 2;
const u3 = buffer.readUInt16LE(offset);
for (let i = 0; i < u3; i++) {
  offset += 2;
  const u3u = buffer.readUInt16LE(offset);
  console.log(u3u);
}
offset += 2;
const u4 = buffer.readUInt32LE(offset);
console.log(u4);
offset += 4;
const u5 = buffer.readUInt32LE(offset);
offset += 4;
const u6 = buffer.readUInt32LE(offset);
offset += 4;
const u7 = buffer.readUInt32LE(offset);
offset += 4;
const u8 = buffer.readUInt32LE(offset);
offset += 4;
let u9 = buffer.readUInt16LE(offset);
while (u9 !== 0xFFFF) {
  console.log("unk", offset.toString(16).padStart(4,0), u9);
  offset += 42;
  u9 = buffer.readUInt16LE(offset);
}
console.log(u9);
offset += 2;
let u10 = buffer.readUInt32LE(offset);
while (u10 !== 0) {
  offset += 0x54;
  u10 = buffer.readUInt32LE(offset);
}
const u11 = buffer.readUInt32LE(offset);
console.log(offset.toString(16).padStart(4,0), u11);
offset += 4;
const u12 = buffer.readUInt16LE(offset);
console.log(u12);
offset += 2;
const u13 = buffer.readUInt32LE(offset);
console.log(u13);
offset += 4;
// Mandanga
let zone = "", zoneRow = "";
for (let y = 0; y < 18; y++) {
  zoneRow = "";
  for (let x = 0; x < 18; x++) {
    const ur = buffer.readUInt16LE(offset);
    offset += 2;
    const ug = buffer.readUInt16LE(offset);
    offset += 2;
    const ub = buffer.readUInt16LE(offset);
    offset += 2;
    zoneRow += (ub !== 0xFFFF) ? "#" : " ";
  }
  zone += `${zoneRow}\n`;
}
console.log(zone);

//
// Mapa
//
const mapOffset = 0x2F6D;
offset = mapOffset;
let map = "", row = "";
for (let y = 0; y < 10; y++) {
  row = "";
  for (let x = 0; x < 10; x ++) {
    const tile = buffer.readUInt16LE(offset);
    if (tile === 0xFFFF) {
      row += "### ";
    } else {
      row += `${tile.toString().padStart(3,"0")} `;
    }
    offset += 42;
  }
  map += `${row}\n`;
}
console.log(map);

//
// Estado de las zonas?
//


//
// Inventario
//
const inventory = [];
const numberOfInventoryItems = buffer.readUInt16LE();
for (let i = 0; i < numberOfInventoryItems; i++) {
  const item = buffer.readUInt16LE();
  inventory.push(item);
}
