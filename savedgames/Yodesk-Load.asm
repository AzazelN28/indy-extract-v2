;
; What The fuck
;
LoadGame_StartReadingLoop_00404330:
        sub    esp, 18h
        mov    [esp+08h], ecx
        push   ebx
        mov    eax, [esp+0ch]
        push   esi
        movsx  ecx, word ptr [esp+28h]
        push   edi
        mov    edx, [eax+98h]

        mov    eax, [esp+30h] ; Mete en EAX lo que hubiera en la estructura ESP+30h

        push   ebp
        mov    ecx, [edx][ecx*4]
        push   eax
        mov    esi, [esp+30h]
        mov    [esp+18h], ecx
        push   esi

        xor    edi, edi ; EDI = 0
        call   LoadGame_ReadSomething_004072e0

        mov    ecx, [esp+14h]
        mov    [esp+1ch], edi
        mov    eax, [ecx+7b0h]
        mov    [esp+20h], eax

        ; if (EAX < EDI) {
        ;   LoadGame_CloseFile_004043d7();
        ; }
        cmp    eax, edi
        jle    LoadGame_CloseFile_004043d7
;                                   XREFS First: 1000:004043d5 Number : 1
LoadGame_ReadSomething_00404378:
        mov    eax, [esp+14h]
        mov    ecx, [eax+7ach]
        mov    eax, [ecx][edi]

        ; if (eax+04h !== 9) {
        ;   LoadGame_004043c6();
        ; } else if (eax+0eh < 0) {
        ;   LoadGame_004043c6();
        ; }
        cmp    dword ptr [eax+04h], 09h
        jnz    LoadGame_004043c6

        mov    bx, [eax+0eh]
        test   bx, bx
        jl     LoadGame_004043c6

        mov    eax, [esi]

        ; readU16();
        push   02h
        lea    ecx, [esp+16h]
        mov    ebp, [eax+3ch]
        push   ecx
        mov    ecx, esi
        call   ebp
        lea    eax, [esp+24h]

        ; readU32();
        push   04h
        push   eax
        mov    ecx, esi
        call   ebp

        ;
        cmp    bx, [esp+12h]
        jnz    LoadGame_CloseFile_004043d7

        mov    eax, [esp+24h]
        mov    ecx, [esp+18h]
        push   eax
        push   ebx
        push   esi
        call   LoadGame_StartReadingLoop_00404330
;                                   XREFS First: 1000:00404389 Number : 2
LoadGame_004043c6:
        ; edi += 4
        add    edi, 04h
        ; (dword ptr [esp+1ch])++
        inc    dword ptr [esp+1ch]
        mov    eax, [esp+1ch]
        cmp    eax, [esp+20h]
        jl     LoadGame_ReadSomething_00404378
;                                   XREFS First: 1000:00404376 Number : 2
LoadGame_CloseFile_004043d7:
        pop    ebp
        pop    edi
        pop    esi
        pop    ebx

        add    esp, 18h

        ret    0ch

LoadGame_ReadSomething_004072e0:
        mov    eax, fs:dword ptr [00h]
        push   ebp
        mov    ebp, esp
        push   0ffh
        push   offset LoadGame_Structure_0040762a
        push   eax
        mov    dword ptr fs:[00h], esp
        sub    esp, 10h

        ; dword ptr [ebp+0ch] - 00h

        push   ebx
        mov    [ebp-10h], ecx
        push   esi
        push   edi
        ; if (dword ptr [ebp+0ch] == 00h) {
        ;   LoadGame_004073a3();
        ; }
        cmp    dword ptr [ebp+0ch], 00h
        jz     LoadGame_004073a3

        mov    eax, [ebp+08h]

        ; readU32();
        push   04h
        mov    ecx, [eax]
        mov    eax, [ebp-10h]
        add    eax, 834h
        mov    ebx, [ecx+3ch]
        push   eax
        mov    ecx, [ebp+08h]
        call   ebx

        ; readU32();
        push   04h
        mov    eax, [ebp-10h]
        add    eax, 838h
        mov    ecx, [ebp+08h]
        push   eax
        call   ebx

        ; readU32();
        push   04h
        mov    eax, [ebp-10h]
        add    eax, 83ch
        mov    ecx, [ebp+08h]
        push   eax
        call   ebx

        ; readU32();
        push   04h
        mov    eax, [ebp-10h]
        add    eax, 840h
        mov    ecx, [ebp+08h]
        push   eax
        call   ebx

        ; readU16();
        push   02h
        mov    eax, [ebp-10h]
        add    eax, 844h
        mov    ecx, [ebp+08h]
        push   eax
        call   ebx

        ; readU16();
        push   02h
        ; metemos en EAX una longitud, la que hay
        ; en [ebp-10h]
        mov    eax, [ebp-10h]
        add    eax, 846h
        mov    ecx, [ebp+08h]
        push   eax
        xor    esi, esi
        call   ebx

        ; Metemos en ECX lo que haya en [ebp-10h] que
        ; al parecer es una longitud de algo.
        mov    ecx, [ebp-10h]

        cmp    [ecx+0ch], si
        jle    LoadGame_004073a3

        lea    edi, [ecx+10h]
;                                   XREFS First: 1000:004073a1 Number : 1
LoadGame_ReadStructureLoop_00407381:
        ; mete en EAX lo que se encuentre en [ebp-10h]
        mov    eax, [ebp-10h]

        ; index++;
        inc    esi

        ; read(x, eax, 1, file);
        movsx  ecx, word ptr [eax+0eh]
        lea    eax, [ecx][ecx*2]
        mov    ecx, [ebp+08h]
        add    eax, eax
        push   eax ; length
        push   edi ; mete en edi+6ch la longitud de EAX
        add    edi, 6ch
        call   ebx

        mov    eax, [ebp-10h]
        movsx  ecx, word ptr [eax+0ch]

        ; while (ecx > esi);
        cmp    ecx, esi
        jg     LoadGame_ReadStructureLoop_00407381
;                                   XREFS First: 1000:00407305 Number : 2
LoadGame_004073a3:
        mov    eax, [ebp+08h]

        ; readU32();
        push   04h
        mov    ecx, [eax]
        mov    eax, [ebp-10h]
        add    eax, 08h
        mov    edi, [ecx+3ch]
        push   eax
        mov    ecx, [ebp+08h]
        call   edi

        ; readU32();
        push   04h
        lea    ecx, [ebp-14h]
        push   ecx
        mov    ecx, [ebp+08h]
        call   edi

        ;
        mov    ecx, [ebp-10h]
        mov    eax, [ecx+7b0h]

        ;
        cmp    [ebp-14h], eax
        jle    LoadGame_00407422

        mov    ebx, [ebp-14h]
        xor    esi, esi
        sub    ebx, eax
        test   ebx, ebx
        jle    LoadGame_00407422
;                                   XREFS First: 1000:00407420 Number : 1
LoadGame_004073dd:
        push   10h
        call   LoadGame_0043932b

        add    esp, 04h
        mov    [ebp-18h], eax
        mov    dword ptr [ebp-04h], 00h

        test   eax, eax
        jz     LoadGame_004073fe

        mov    ecx, eax
        call   LoadGame_004065f0
        jmp    LoadGame_00407400

LoadGame_004065f0:
        mov    eax, fs:dword ptr [00h]
        push   ebp
        mov    ebp, esp
        push   0ffh
        push   offset loc_0040664e
        push   eax
        mov    dword ptr fs:[00h], esp
        xor    eax, eax
        sub    esp, 04h
        mov    [ebp-04h], eax
        push   esi
        mov    [ebp-10h], ecx
        mov    [ecx+0ch], ax
        mov    dword ptr [ecx], offset loc_0044d504
        mov    [ecx+0ah], ax
        mov    dword ptr [ecx], offset loc_0044c698
        mov    [ecx+08h], ax
        mov    [ecx+04h], eax
        mov    esi, ecx
        mov    eax, 0ffffffffh
        mov    [esi+0eh], ax
        mov    [ebp-04h], eax
        mov    eax, esi
        mov    ecx, [ebp-0ch]
        mov    dword ptr fs:[00h], ecx
        pop    esi
        mov    esp, ebp
        pop    ebp
        ret

        ;                                   XREFS First: 1000:004073f3 Number : 1
LoadGame_004073fe:
        xor    eax, eax
;                                   XREFS First: 1000:004073fc Number : 1
LoadGame_00407400:
        push   eax

        ; esi++;
        inc    esi

        mov    eax, [ebp-10h]
        mov    dword ptr [ebp-04h], 0ffffffffh
        mov    ecx, [eax+7b0h]
        push   ecx
        lea    ecx, [eax+7a8h]
        call   LoadGame_00438463

        cmp    esi, ebx
        jl     LoadGame_004073dd
;                                   XREFS First: 1000:004073d0 Number : 2
LoadGame_00407422:
        xor    esi, esi ; ESI = 0
        mov    [ebp-18h], esi

        ; if ([ebp-14] < 0) {
        ;   LoadGame_0040747e();
        ; }
        cmp    [ebp-14h], esi
        jle    LoadGame_0040747e
;                                   XREFS First: 1000:0040747c Number : 1
LoadGame_0040742c:
        mov    eax, [ebp-10h]
        ; readU16();
        push   02h
        add    esi, 04h
        mov    ecx, [eax+7ach]
        mov    ebx, [ecx-04h][esi]
        mov    ecx, [ebp+08h]
        lea    eax, [ebx+08h]
        add    ebx, 0ch
        push   eax
        call   edi

        ; readU16();
        push   02h
        lea    eax, [ebx+02h]
        push   eax
        mov    ecx, [ebp+08h]
        call   edi

        ; readU32();
        push   04h
        lea    eax, [ebx-08h]
        push   eax
        mov    ecx, [ebp+08h]
        call   edi

        ; readU16();
        push   02h
        lea    eax, [ebx-02h]
        push   eax
        mov    ecx, [ebp+08h]
        call   edi

        ; readU16();
        push   02h
        mov    ecx, [ebp+08h]
        push   ebx
        call   edi

        ; (dword ptr [ebp-18h])++
        inc    dword ptr [ebp-18h]
        mov    eax, [ebp-18h]

        cmp    [ebp-14h], eax
        jg     LoadGame_0040742c
;                                   XREFS First: 1000:0040742a Number : 1
LoadGame_0040747e:
        xor    esi, esi ; resetea ESI (ESI = 0)

        ; if ([ebp+0ch] === 0) {
        ;   LoadGame_FailedExit_00407618();
        ; }
        cmp    [ebp+0ch], esi
        jz     LoadGame_FailedExit_00407618

        ; readU32()
        push   04h
        lea    eax, [ebp-14h]
        push   eax
        mov    ecx, [ebp+08h]
        call   edi

        mov    [ebp-18h], esi

        ; if ([ebp-14h] < 0) {
        ;   LoadGame_004075e3();
        ; }
        cmp    [ebp-14h], esi
        jle    LoadGame_004075e3

        mov    [ebp-1ch], esi
;                                   XREFS First: 1000:004075dd Number : 1
LoadGame_004074a3:
        mov    eax, [ebp-10h]
        mov    edx, [ebp-1ch]
        ; readU16();
        push   02h
        mov    ecx, [eax+7d4h]
        mov    esi, [ecx][edx]
        mov    ecx, [ebp+08h]
        lea    eax, [esi+04h]
        push   eax
        call   edi
        ; readU16();
        push   02h
        lea    eax, [esi+06h]
        push   eax
        mov    ecx, [ebp+08h]
        call   edi
        ; readU16();
        push   02h
        lea    eax, [esi+08h]
        push   eax
        mov    ecx, [ebp+08h]
        call   edi
        ; readU16();
        push   02h
        lea    eax, [esi+0ah]
        push   eax
        mov    ecx, [ebp+08h]
        call   edi
        ; readU32();
        push   04h
        lea    eax, [esi+0ch]
        push   eax
        mov    ecx, [ebp+08h]
        call   edi
        ; readU16();
        push   02h
        lea    eax, [esi+10h]
        push   eax
        mov    ecx, [ebp+08h]
        call   edi
        ; readU16();
        push   02h
        lea    eax, [esi+12h]
        push   eax
        mov    ecx, [ebp+08h]
        call   edi
        ; readU16();
        push   02h
        lea    eax, [esi+14h]
        push   eax
        mov    ecx, [ebp+08h]
        call   edi
        ; readU16();
        push   02h
        lea    eax, [esi+16h]
        push   eax
        mov    ecx, [ebp+08h]
        call   edi
        ; readU32();
        push   04h
        lea    eax, [esi+18h]
        push   eax
        mov    ecx, [ebp+08h]
        call   edi
        ; readU32();
        push   04h
        lea    eax, [esi+1ch]
        push   eax
        mov    ecx, [ebp+08h]
        call   edi
        ; readU32();
        push   04h
        lea    eax, [esi+20h]
        push   eax
        mov    ecx, [ebp+08h]
        call   edi
        ; readU16();
        push   02h
        lea    eax, [esi+38h]
        push   eax
        mov    ecx, [ebp+08h]
        call   edi
        ; readU16();
        push   02h
        lea    eax, [esi+3ah]
        push   eax
        mov    ecx, [ebp+08h]
        call   edi
        ; readU16();
        push   02h
        lea    eax, [esi+3ch]
        push   eax
        mov    ecx, [ebp+08h]
        call   edi
        ; readU16();
        push   02h
        lea    eax, [esi+3eh]
        push   eax
        mov    ecx, [ebp+08h]
        call   edi
        ; readU16();
        push   02h
        lea    eax, [esi+60h]
        push   eax
        mov    ecx, [ebp+08h]
        call   edi
        ; readU16();
        push   02h
        lea    eax, [esi+26h]
        push   eax
        mov    ecx, [ebp+08h]
        call   edi
        ; readU32();
        push   04h
        lea    eax, [esi+2ch]
        push   eax
        mov    ecx, [ebp+08h]
        call   edi
        ; readU32();
        push   04h
        lea    eax, [esi+34h]
        push   eax
        mov    ecx, [ebp+08h]
        call   edi
        ; readU32();
        push   04h
        lea    eax, [esi+28h]
        push   eax
        mov    ecx, [ebp+08h]
        call   edi
        ; readU16();
        push   02h
        lea    eax, [esi+24h]
        push   eax
        mov    ecx, [ebp+08h]
        add    esi, 40h
        call   edi
        ; readU16();
        push   02h
        mov    ecx, [ebp+08h]
        lea    eax, [esi-10h]

        ; ebx = 4
        mov    ebx, 04h

        push   eax
        call   edi
;                                   XREFS First: 1000:004075ce Number : 1
LoadGame_004075b7:
        ; readU32();
        push   04h
        mov    ecx, [ebp+08h]
        push   esi
        call   edi
        lea    eax, [esi+04h]

        ; readU32();
        push   04h
        push   eax
        mov    ecx, [ebp+08h]
        add    esi, 08h
        call   edi

        ; ebx--
        dec    ebx

        ; while (y != 0);
        jnz    LoadGame_004075b7

        ; dword ptr [ebp-1ch] += 4
        add    dword ptr [ebp-1ch], 04h

        ; dword ptr [ebp-18h]++;
        inc    dword ptr [ebp-18h]
        mov    eax, [ebp-18h]

        ; if ([ebp-14h] > [ebp-18h])
        ;   LoadGame_004074a3();
        cmp    [ebp-14h], eax
        jg     LoadGame_004074a3
;                                   XREFS First: 1000:0040749a Number : 1
;
;
;
LoadGame_004075e3:
        ; readU32();
        push   04h
        lea    eax, [ebp-14h]
        push   eax
        mov    ecx, [ebp+08h]
        xor    ebx, ebx ; RESETEA EBX (EBX = 0)
        call   edi

        xor    esi, esi ; RESETEA ESI (ESI = 0)

        ; if ([ebp-14h] < 0) {
        ;   LoadGame_FailedExit_00407618();
        ; }
        cmp    [ebp-14h], ebx
        jle    LoadGame_FailedExit_00407618
;                                   XREFS First: 1000:00407616 Number : 1
LoadGame_004075f7:
        ; readU32();
        mov    eax, [ebp-10h]
        push   04h
        mov    ecx, [eax+7c0h]
        add    ebx, 04h

        mov    eax, [ecx-04h][ebx]
        mov    ecx, [ebp+08h]
        add    eax, 2ch
        push   eax
        call   edi

        ; esi++;
        inc    esi

        ; if ([ebp-14h] > esi) {
        ;   LoadGame_004075f7();
        ; }
        ;
        ; En realidad esto es un:
        ;
        ; while ([ebp-14h] > esi);
        ;
        cmp    [ebp-14h], esi
        jg     LoadGame_004075f7
;                                   XREFS First: 1000:00407483 Number : 2
;
; Hace pop de edi, esi, ebx y ebp, seguramente sea
; una finalización de algo.
; Retorna 8
;
LoadGame_FailedExit_00407618:
        mov    eax, [ebp-0ch]
        pop    edi
        mov    fs:dword ptr [00h], eax
        pop    esi
        pop    ebx
        mov    esp, ebp
        pop    ebp
        ret    08h
;                                   XREFS First: 1000:004012de Number : 69
LoadGame_0043932b:
        push   esi
        push   edi
        mov    edi, dword ptr [loc_0044d518]
;                                   XREFS First: 1000:00439364 Number : 1
LoadGame_00439333:
        push   dword ptr [esp+0ch]
        call   LoadGame_0042b7d0
        add    esp, 04h
        mov    esi, eax

        test   esi, esi
        jnz    LoadGame_00439366

        cmp    dword ptr [loc_0044d518], edi
        jnz    LoadGame_00439355

        call   LoadGame_004497ee
        mov    edi, [eax+28h]
;                                   XREFS First: 1000:0043934b Number : 1
LoadGame_00439355:
        test   edi, edi
        jz     LoadGame_00439366
        push   dword ptr [esp+0ch]
        call   edi
        add    esp, 04h
        test   eax, eax
        jnz    LoadGame_00439333
;                                   XREFS First: 1000:00439343 Number : 2
LoadGame_00439366:
        mov    eax, esi
        pop    edi
        pop    esi
        ret
;                                   XREFS First: 1000:00401245 Number : 40
LoadGame_0043936b:
        push   dword ptr [esp+04h]
        call   LoadGame_0042b8a0
        add    esp, 04h
        ret

LoadGame_004497ee:
        call   LoadGame_004497c8
        lea    ecx, [eax+1070h]
        push   offset loc_00429721
        call   LoadGame_0044a1f5
        ret
LoadGame_004065f0:
        mov    eax, fs:dword ptr [00h]
        push   ebp
        mov    ebp, esp
        push   0ffh
        push   offset loc_0040664e
        push   eax
        mov    dword ptr fs:[00h], esp
        xor    eax, eax
        sub    esp, 04h
        mov    [ebp-04h], eax
        push   esi
        mov    [ebp-10h], ecx
        mov    [ecx+0ch], ax
        mov    dword ptr [ecx], offset loc_0044d504
        mov    [ecx+0ah], ax
        mov    dword ptr [ecx], offset loc_0044c698
        mov    [ecx+08h], ax
        mov    [ecx+04h], eax
        mov    esi, ecx
        mov    eax, 0ffffffffh
        mov    [esi+0eh], ax
        mov    [ebp-04h], eax
        mov    eax, esi
        mov    ecx, [ebp-0ch]
        mov    dword ptr fs:[00h], ecx
        pop    esi
        mov    esp, ebp
        pop    ebp
        ret
LoadGame_004497c8:
        push   offset LoadGame_Structure_00429548
        mov    ecx, offset LoadGame_Structure_Uint32_0045c890
        call   LoadGame_0044a1f5
        mov    eax, [eax+04h]
        test   eax, eax
        jnz    loc_004497ed
        push   offset loc_004298f5
        mov    ecx, offset loc_0045c894
        call   loc_0044a2cc
;                                   XREFS First: 1000:004497dc Number : 1
loc_004497ed:
        ret
LoadGame_0044a1f5:
        push   esi
        push   edi
        cmp    dword ptr [ecx], 00h
        mov    esi, ecx
        jnz    loc_0044a233
        cmp    dword ptr [loc_0045cbe8], 00h
        jnz    loc_0044a226
        mov    ecx, offset loc_0045cbf0
        test   ecx, ecx
        jz     loc_0044a21c
        call   loc_00449dce
        mov    dword ptr [loc_0045cbe8], eax
        jmp    loc_0044a226
;                                   XREFS First: 1000:0044a20e Number : 1
loc_0044a21c:
        mov    dword ptr [loc_0045cbe8], 00h
;                                   XREFS First: 1000:0044a205 Number : 2
loc_0044a226:
        mov    ecx, dword ptr [loc_0045cbe8]
        call   loc_00449e0d
        mov    [esi], eax
;                                   XREFS First: 1000:0044a1fc Number : 1
loc_0044a233:
        mov    edi, [esi]
        mov    eax, dword ptr [loc_0045cbe8]
        push   dword ptr [eax]
        call   dword ptr [TlsGetValue]

        test   eax, eax
        jz     loc_0044a253

        cmp    [eax+08h], edi
        jle    loc_0044a253
        mov    eax, [eax+0ch]
        mov    edi, [eax][edi*4]
        jmp    loc_0044a255
;                                   XREFS First: 1000:0044a244 Number : 2
loc_0044a253:
        xor    edi, edi
;                                   XREFS First: 1000:0044a251 Number : 1
loc_0044a255:
        test   edi, edi
        jnz    loc_0044a26d
        call   dword ptr [esp+0ch]
        push   eax
        mov    edi, eax
        push   dword ptr [esi]
        mov    ecx, dword ptr [loc_0045cbe8]
        call   loc_00449f78
;                                   XREFS First: 1000:0044a257 Number : 1
loc_0044a26d:
        mov    eax, edi
        pop    edi
        pop    esi
        ret    04h
