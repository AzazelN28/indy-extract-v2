section .text
  global _start

_start:
  mov ecx, 02h
  mov edx, 02h
  add edx, ecx

  cmp edx, 04h
  jz ok

ko:
  mov eax, 1
  mov ebx, 1
  int 0x80

ok:
  mov eax, 1
  mov ebx, 0
  int 0x80
