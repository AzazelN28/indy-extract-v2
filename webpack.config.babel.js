import webpack from "webpack";
import path from "path";

import ExtractTextPlugin from "extract-text-webpack-plugin";
import HtmlPlugin from "html-webpack-plugin";

function src(...args) {
  return path.resolve(__dirname, "src", ...args);
}

function dist(...args) {
  return path.resolve(__dirname, "dist", ...args);
}

const plugins = (() => {
  const basePlugins = [
    new ExtractTextPlugin("index.css"),
    new HtmlPlugin({
      title: "Yoda Stories",
      template: src("index.pug")
    }),
    new webpack.DefinePlugin({
      "process.env.NODE_ENV": JSON.stringify(process.env.NODE_ENV)
    })
  ];
  if (process.env.NODE_ENV === "development") {
    return basePlugins.concat([
      new webpack.HotModuleReplacementPlugin()
    ]);
  }
  return basePlugins.concat([
    new webpack.optimize.UglifyJsPlugin({
      global: true,
      mangle: true,
      compress: true
    })
  ]);
})();

const config = {
  entry: [
    src("index.js"),
    src("index.styl")
  ],

  output: {
    path: dist(),
    filename: "index.js"
  },

  module: {
    rules: [
      {
        test: /\.jsx?$/,
        use: "babel-loader"
      },
      {
        test: /\.styl$/,
        use: ExtractTextPlugin.extract({
          use: ["css-loader", "stylus-loader"]
        })
      },
      {
        test: /\.pug$/,
        use: "pug-loader"
      }
    ]
  },

  plugins,

  devServer: {
    contentBase: dist(),
    compress: true,
    hot: true,
    port: 4000
  }
};

export default config;
