import { render } from "react-dom";

function Application(props) {
  return (
    <div>Hello, World</div>
  )
}

render(
  <Application />,
  document.querySelector("#root")
);
