module.exports = function(v, length, chr = "0") {
  let str = String(v);
  while (str.length < length) {
    str = chr + str;
  }
  return str;
};
