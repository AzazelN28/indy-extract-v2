
class Reader {
  constructor(buffer, isLittleEndian = false) {
    this.buffer = buffer;
    this.offset = 0;
    this.stack = [];
    this.isLittleEndian = isLittleEndian;
  }

  get isComplete() {
    return this.available <= 0;
  }

  get available() {
    if (this.stack.length > 0) {
      return this.stack[this.stack.length - 1] - this.offset;
    }
    return this.buffer.byteLength - this.offset;
  }

  readExpectedID4(id4) {
    const b0 = this.buffer.readUInt8(this.offset);
    this.offset ++;
    const b1 = this.buffer.readUInt8(this.offset);
    this.offset ++;
    const b2 = this.buffer.readUInt8(this.offset);
    this.offset ++;
    const b3 = this.buffer.readUInt8(this.offset);
    this.offset ++;
    return id4.charCodeAt(0) === b0
        && id4.charCodeAt(1) === b1
        && id4.charCodeAt(2) === b2
        && id4.charCodeAt(3) === b3;
  }

  readID4() {
    const b0 = this.buffer.readUInt8(this.offset);
    this.offset ++;
    const b1 = this.buffer.readUInt8(this.offset);
    this.offset ++;
    const b2 = this.buffer.readUInt8(this.offset);
    this.offset ++;
    const b3 = this.buffer.readUInt8(this.offset);
    this.offset ++;
    //console.log(b0.toString(16), b1.toString(16), b2.toString(16), b3.toString(16));
    const id4 = String.fromCharCode(b0)
              + String.fromCharCode(b1)
              + String.fromCharCode(b2)
              + String.fromCharCode(b3);
    return id4;
  }

  readString(length) {
    let result = "";
    let shouldSkip = false;
    for (let index = 0; index < length; index++) {
      const character = this.buffer.readUInt8(this.offset);
      if (character === 0x00 && shouldSkip === false) {
        shouldSkip = true;
      }
      if (!shouldSkip) {
        result += String.fromCharCode(character);
      }
      this.offset++;
    }
    return result;
  }

  readBytes(length) {
    const newBuffer = this.buffer.slice(this.offset, this.offset + length);
    this.offset += length;
    return newBuffer;
  }

  readInt8() {
    const result = this.buffer.readInt8(this.offset);
    this.offset++;
    return result;
  }

  readInt16(littleEndian = this.littleEndian) {
    const result = littleEndian ? this.buffer.readInt16LE(this.offset) : this.buffer.readInt16BE(this.offset);
    this.offset += 2;
    return result;
  }

  readInt32(littleEndian = this.littleEndian) {
    const result = littleEndian ? this.buffer.readInt32LE(this.offset) : this.buffer.readInt32BE(this.offset);
    this.offset += 4;
    return result;
  }

  readUint8() {
    const result = this.buffer.readUInt8(this.offset);
    this.offset++;
    return result;
  }

  readUint16(littleEndian = this.littleEndian) {
    const result = littleEndian ? this.buffer.readUInt16LE(this.offset) : this.buffer.readUInt16BE(this.offset);
    this.offset += 2;
    return result;
  }

  readUint32(littleEndian = this.littleEndian) {
    const result = littleEndian ? this.buffer.readUInt32LE(this.offset) : this.buffer.readUInt32BE(this.offset);
    this.offset += 4;
    return result;
  }

  readFloat(littleEndian = this.littleEndian) {
    const result = littleEndian ? this.buffer.readFloatLE(this.offset) : this.buffer.readFloatBE(this.offset);
    this.offset += 4;
    return result;
  }

  readDouble(littleEndian = this.littleEndian) {
    const result = littleEndian ? this.buffer.readDoubleLE(this.offset) : this.buffer.readDoubleBE(this.offset);
    this.offset += 8;
    return result;
  }

  readInt8AndSkip() {
    const size = this.readInt8();
    return this.skip(size);
  }

  readInt16AndSkip(littleEndian = this.littleEndian) {
    const size = this.readInt16(littleEndian);
    return this.skip(size);
  }

  readInt32AndSkip(littleEndian = this.littleEndian) {
    const size = this.readInt32(littleEndian);
    return this.skip(size);
  }

  readUint8AndSkip() {
    const size = this.readUint8();
    return this.skip(size);
  }

  readUint16AndSkip(littleEndian = this.littleEndian) {
    const size = this.readUint16(littleEndian);
    return this.skip(size);
  }

  readUint32AndSkip(littleEndian = this.littleEndian) {
    const size = this.readUint32(littleEndian);
    return this.skip(size);
  }

  skip(bytes) {
    return this.offset += bytes;
  }

  seek(newOffset, type = 0) {
    if (type === 0) {
      this.offset += newOffset;
    } else if (type === 1) {
      this.offset = newOffset;
    } else if (type === 2) {
      this.offset = this.buffer.byteLength - newOffset;
    }
    return this.offset;
  }

  slice(length) {
    return new Reader(this.buffer.slice(this.offset, this.offset + length));
  }

  push(size) {
    this.stack.push(this.offset + size);
  }

  pop() {
    const available = this.available;
    this.stack.pop();
    return available;
  }
}

Reader.CURRENT = 0;
Reader.BEGIN = 1;
Reader.END = 2;

module.exports = Reader;
