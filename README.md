# Desktop Adventures

## Yoda Stories

Número de sonidos:      63
Número de tiles:      2122
Número de zonas:       657
Número de puzzles:     204

## Generación del escenario

NOTA: Siempre se comienza la partida en Dagobah así que realmente no hace falta
incluir estos mapas en la generación del mundo.

1. Se escoge _aleatoriamente_ uno de los 15 puzzles iniciales.
2. Cada _puzzle inicial_ lleva asociada una _zona de inicio_ (esto debe ser así principalmente porque
en los textos de Yoda muchas veces se indica que es un planeta helado o es un planeta desértico así
que deben estar guardados estos enlaces de alguna manera).
3. Esta zona inicial posee un _tipo de mundo_ con lo cuál podemos generar el resto de zonas adyacentes
a partir de esta zona inicial.
4. Las zonas deben poseer un tipo o un flag que indica qué tipo de zona es, es decir, hay zonas que
poseen puzzles y hay zonas que no. Yoda NUNCA comienza la partida diciéndote exactamente cuál es el
primer item, principalmente porque éste primer item es en realidad el item necesario para la primera zona.
Así que una vez que ponemos la primera zona de puzzle, elegimos el primer puzzle (que normalmente requerirá
el primer objeto y soltará otro objeto que se usará para el siguiente puzzle). Cuando generemos otra zona
podremos obtener otro puzzle y ese irá encajado con el siguiente.

## Puzzles

Las cadenas de texto suelen ser:
  1. Petición
  2. Agradecimiento
  3. Ofrecimiento
  4. Comienzo

Cuando unknown1 es 0 indica que es un puzzle de dar o recibir.
  - Existen las tres primeras cadenas de texto.
Cuando unknown1 es 1 indica que es un puzzle de dar.
  - Sólo existe la tercera cadena de texto.
Cuando unknown1 es 2 indica que es un puzzle de dar (especial porque todos tienen agradecimiento).
  - Pueden existir las tres primeras pero casi seguro que sólo existe la segunda y la tercera.
Cuando unknown1 es 3 indica que es un puzzle de arranque.

NOTA: Es MUY posible que el campo unknown2 sea algún tipo de Flag.
- Cuando unknown2 es 0 indica que se necesita un KEY CARD.
- Cuando unknown2 es 1 o 2 indica que hay intercambio de items.
- Cuando unknown2 es 4 no tengo ni idea de lo que implica.

- unknown3 en su mayoría es 0.
- unknown3 sólo vale 2 en un caso y curiosamente parece que involucra a Chewbacca y tiene dos textos que parece que no se muestran nunca.
- unknown3 sólo vale 3 dos veces y no parece tener ninguna característica especial.
- unknown3 vale 4 en todos los puzzles de arranque.
- unknown3 en el resto de casos vale 0xffffffff.

NOTA: Tiene pinta de que el campo unknown4 también es algún tipo de flag.
NOTA: Creo que el primer bit del primer byte puede indicar que requiere un item.
NOTA: Los bits del segundo byte puede indicar con qué puzzles coincidiría o qué tipo de puzzles son compatibles con este.
- unknown4 en los puzzles de arranque vale 0
- unknown4 en los puzzles de tipo 2 vale 128
- unknown4 en el resto de puzzles de tipo 0 y 1 vale 286,287,279,311,etc. (recordemos que esto parecen flags)

IMPORTANTE: No parece que los puzzles tengan relación alguna con los planetas. Al menos los puzzles iniciales.
¿Es posible que esto se pueda deducir por el número de final que les corresponde?

Tiene pinta de que cuando se construye el juego, se elige entre uno de los posibles 15 inicios, una vez que
hemos elegido un inicio podemos elegir entre 2 posibles finales para ese inicio.

Los finales tienen los siguientes IDs.

0810 'END1A'
0816 'END1B'

1353 'END2A'
1354 'END2B'

1596 'END3A'
1597 'END3B'

1617 'END4A'
1618 'END4B'

1788 'END5A'
1789 'END5B'

0448 'END6A'? EL TILE ES UNA LINTERNA DE LUZ SAGRADA
1984 'END6B'

1935 'End7A'
1936 'End7B'

0529 'End8A'
0610 'END8B'? EL TILE ES UN AMULETO SITH

0459 'END9A'? EL TILE ES UN CRISTAL ADEGAN
1983 'END9B'

1828 'END10A'
1829 'END10B'

1985 'END11A'
1986 'END11B'

1650 'END12A'
1651 'END12B'

1378 'END13A'
1379 'END13B'

???? 'END14A'
???? 'END14B'

1419 'END15A'
1420 'END15B'

1652 'END16A'
1653 'END16B'

La zona 476 y la 573 hacen un check del item 1788
